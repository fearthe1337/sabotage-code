package com.sabotage.kNoAPP;

import java.util.ArrayList;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;

public class IronBar {
	public static ArrayList<IronBar> ironbars = new ArrayList<IronBar>();
	
	private Map map;
	private int id;
	private int x;
	private int y;
	private int z;

	public IronBar(Map map, int id, int x, int y, int z) {
		this.map = map;
		this.id = id;
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public static void createIronBar(Map map, int x, int y, int z) {
		FileConfiguration fc = Data.MAIN.getFileConfig();
		int i = getNextOpenID(map);
		fc.set("Maps.MapIDs." + map.getID() + ".IronBars.IronBarIDs." + i + ".X", x);
		fc.set("Maps.MapIDs." + map.getID() + ".IronBars.IronBarIDs." + i + ".Y", y);
		fc.set("Maps.MapIDs." + map.getID() + ".IronBars.IronBarIDs." + i + ".Z", z);
		fc.set("Maps.MapIDs." + map.getID() + ".IronBars.IronBarCount", fc.getInt("Maps.MapIDs." + map.getID() + ".IronBars.IronBarCount") + 1);
		Data.MAIN.saveDataFile(fc);
		refreshIronBars();
	}
	
	public static void removeIronBar(Location l) {
		FileConfiguration fc = Data.MAIN.getFileConfig();
		for(IronBar IronBar : ironbars) {
			if(SABTools.compareLocationXYZ(IronBar.getX(), IronBar.getY(), IronBar.getZ(), l)) {
				fc.set("Maps.MapIDs." + IronBar.getMap().getID() + ".IronBars.IronBarIDs." + IronBar.getID(), null);
				fc.set("Maps.MapIDs." + IronBar.getMap().getID() + ".IronBars.IronBarCount", fc.getInt("Maps.MapIDs." + IronBar.getMap().getID() + ".IronBars.IronBarCount") - 1);
			}
		}
		Data.MAIN.saveDataFile(fc);
		refreshIronBars();
	}
	
	public static IronBar getIronBarByLocation(Location l) {
		for(IronBar IronBar : ironbars) {
			if(SABTools.compareLocationXYZ(IronBar.getX(), IronBar.getY(), IronBar.getZ(), l)) {
				return IronBar;
			}
		}
		return null;
	}
	
	public static boolean validIronBar(Location l) {
		int ax = l.getBlockX();
		int ay = l.getBlockY();
		int az = l.getBlockZ();
		for(IronBar IronBar : ironbars) {
			if(Data.MAIN.getFileConfig().getDouble("Maps.MapIDs." + IronBar.getMap().getID() + ".IronBars.IronBarIDs." + IronBar.getID() + ".X") == ax && Data.MAIN.getFileConfig().getDouble("Maps.MapIDs." + IronBar.getMap().getID() + ".IronBar.IronBarIDs." + IronBar.getID() + ".Y") == ay && Data.MAIN.getFileConfig().getDouble("Maps.MapIDs." + IronBar.getMap().getID() + ".IronBar.IronBarIDs." + IronBar.getID() + ".Z") == az) {
				return true;
			}
		}
		return false;
	}
	
	public static int getNextOpenID(Map map) {
		int currentID = 1;
		while(currentID < 10000) {
			if(!Data.MAIN.getFileConfig().isSet("Maps.MapIDs." + map.getID() + ".IronBars.IronBarIDs." + currentID)) {
				return currentID;
			}
			currentID++;
		}
		return 0;
	}
	
	public Map getMap() {
		return map;
	}
	
	public int getID() {
		return id;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public int getZ() {
		return z;
	}
	
	public static void refreshIronBars() {
		ironbars = importIronBars(Data.MAIN.getFileConfig());
	}
	
	public static ArrayList<IronBar> importIronBars(FileConfiguration fc) {
		ArrayList<IronBar> IronBars = new ArrayList<IronBar>();
		for(Map map : Map.maps) {
			int ibc = fc.getInt("Maps.MapIDs." + map.getID() + ".IronBars.IronBarCount");
			for(int i=1; i<=ibc; i++) {
				if(!fc.isSet("Maps.MapIDs." + map.getID() + ".IronBars.IronBarIDs." + i)) {
					ibc++;
					continue;
				}
				int x = 0;
				int y = 0;
				int z = 0;
				if(fc.isSet("Maps.MapIDs." + map.getID() + ".IronBars.IronBarIDs." + i + ".X")) {
					x = fc.getInt("Maps.MapIDs." + map.getID() + ".IronBars.IronBarIDs." + i + ".X");
				}
				if(fc.isSet("Maps.MapIDs." + map.getID() + ".IronBars.IronBarIDs." + i + ".Y")) {
					y = fc.getInt("Maps.MapIDs." + map.getID() + ".IronBars.IronBarIDs." + i + ".Y");
				}
				if(fc.isSet("Maps.MapIDs." + map.getID() + ".IronBars.IronBarIDs." + i + ".Z")) {
					z = fc.getInt("Maps.MapIDs." + map.getID() + ".IronBars.IronBarIDs." + i + ".Z");
				}
				
				IronBar IronBar = new IronBar(map, i, x, y, z);
				IronBars.add(IronBar);
			}
		}
		return IronBars;
	}
	
	@SuppressWarnings("deprecation")
	public static void loadIronBars(Map m, boolean show) {
		for(IronBar ib : ironbars) {
			if(ib.getMap() == m) {
				if(show) {
					ib.getMap().getSpawnLocation().getWorld().getBlockAt(ib.getX(), ib.getY(), ib.getZ()).setType(Material.getMaterial(101));
				} else {
					ib.getMap().getSpawnLocation().getWorld().getBlockAt(ib.getX(), ib.getY(), ib.getZ()).setType(Material.AIR);
				}
			}
		}
	}
}
