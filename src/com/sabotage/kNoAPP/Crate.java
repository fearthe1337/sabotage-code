package com.sabotage.kNoAPP;

import java.util.ArrayList;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;

public class Crate {
	public static ArrayList<Crate> crates = new ArrayList<Crate>();
	
	private Map map;
	private int id;
	private int x;
	private int y;
	private int z;
	private int level;
	private byte data;

	public Crate(Map map, int id, int x, int y, int z, int level, byte data) {
		this.map = map;
		this.id = id;
		this.x = x;
		this.y = y;
		this.z = z;
		this.level = level;
		this.data = data;
	}
	
	public static void createCrate(Map map, int x, int y, int z, Material m, byte data) {
		FileConfiguration fc = Data.MAIN.getFileConfig();
		int i = getNextOpenID(map);
		fc.set("Maps.MapIDs." + map.getID() + ".Crates.CrateIDs." + i + ".X", x);
		fc.set("Maps.MapIDs." + map.getID() + ".Crates.CrateIDs." + i + ".Y", y);
		fc.set("Maps.MapIDs." + map.getID() + ".Crates.CrateIDs." + i + ".Z", z);
		int level = 0;
		if(m == Material.CHEST) {
			level = 1;
		}
		if(m == Material.ENDER_CHEST) {
			level = 2;
		}
		fc.set("Maps.MapIDs." + map.getID() + ".Crates.CrateIDs." + i + ".Level", level);
		fc.set("Maps.MapIDs." + map.getID() + ".Crates.CrateIDs." + i + ".Data", data);
		fc.set("Maps.MapIDs." + map.getID() + ".Crates.CrateCount", fc.getInt("Maps.MapIDs." + map.getID() + ".Crates.CrateCount") + 1);
		Data.MAIN.saveDataFile(fc);
		refreshCrates();
	}
	
	public static void removeCrate(Location l) {
		FileConfiguration fc = Data.MAIN.getFileConfig();
		for(Crate crate : importCrates(Data.MAIN.getFileConfig())) {
			if(SABTools.compareLocationXYZ(crate.getX(), crate.getY(), crate.getZ(), l)) {
				fc.set("Maps.MapIDs." + crate.getMap().getID() + ".Crates.CrateIDs." + crate.getID(), null);
				fc.set("Maps.MapIDs." + crate.getMap().getID() + ".Crates.CrateCount", fc.getInt("Maps.MapIDs." + crate.getMap().getID() + ".Crates.CrateCount") - 1);
			}
		}
		Data.MAIN.saveDataFile(fc);
		refreshCrates();
	}
	
	public static Crate getCrateByLocation(Location l) {
		for(Crate crate : importCrates(Data.MAIN.getFileConfig())) {
			if(SABTools.compareLocationXYZ(crate.getX(), crate.getY(), crate.getZ(), l)) {
				return crate;
			}
		}
		return null;
	}
	
	public static boolean validCrate(Location l) {
		int ax = l.getBlockX();
		int ay = l.getBlockY();
		int az = l.getBlockZ();
		for(Crate crate : importCrates(Data.MAIN.getFileConfig())) {
			if(Data.MAIN.getFileConfig().getDouble("Maps.MapIDs." + crate.getMap().getID() + ".Crates.CrateIDs." + crate.getID() + ".X") == ax && Data.MAIN.getFileConfig().getDouble("Maps.MapIDs." + crate.getMap().getID() + ".Crates.CrateIDs." + crate.getID() + ".Y") == ay && Data.MAIN.getFileConfig().getDouble("Maps.MapIDs." + crate.getMap().getID() + ".Crates.CrateIDs." + crate.getID() + ".Z") == az) {
				return true;
			}
		}
		return false;
	}
	
	public static int getNextOpenID(Map map) {
		int currentID = 1;
		while(currentID < 10000) {
			if(!Data.MAIN.getFileConfig().isSet("Maps.MapIDs." + map.getID() + ".Crates.CrateIDs." + currentID)) {
				return currentID;
			}
			currentID++;
		}
		return 0;
	}
	
	public Map getMap() {
		return map;
	}
	
	public int getID() {
		return id;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public int getZ() {
		return z;
	}
	
	public int getLevel() {
		return level;
	}
	
	public byte getData() {
		return data;
	}
	
	public static void refreshCrates() {
		crates = importCrates(Data.MAIN.getFileConfig());
	}
	
	public static ArrayList<Crate> importCrates(FileConfiguration fc) {
		ArrayList<Crate> crates = new ArrayList<Crate>();
		for(Map map : Map.maps) {
			int cc = fc.getInt("Maps.MapIDs." + map.getID() + ".Crates.CrateCount");
			for(int i=1; i<=cc; i++) {
				if(!fc.isSet("Maps.MapIDs." + map.getID() + ".Crates.CrateIDs." + i)) {
					cc++;
					continue;
				}
				int x = 0;
				int y = 0;
				int z = 0;
				int level = 0;
				byte data = 0;
				if(fc.isSet("Maps.MapIDs." + map.getID() + ".Crates.CrateIDs." + i + ".X")) {
					x = fc.getInt("Maps.MapIDs." + map.getID() + ".Crates.CrateIDs." + i + ".X");
				}
				if(fc.isSet("Maps.MapIDs." + map.getID() + ".Crates.CrateIDs." + i + ".Y")) {
					y = fc.getInt("Maps.MapIDs." + map.getID() + ".Crates.CrateIDs." + i + ".Y");
				}
				if(fc.isSet("Maps.MapIDs." + map.getID() + ".Crates.CrateIDs." + i + ".Z")) {
					z = fc.getInt("Maps.MapIDs." + map.getID() + ".Crates.CrateIDs." + i + ".Z");
				}
				if(fc.isSet("Maps.MapIDs." + map.getID() + ".Crates.CrateIDs." + i + ".Level")) {
					level = fc.getInt("Maps.MapIDs." + map.getID() + ".Crates.CrateIDs." + i + ".Level");
				}
				if(fc.isSet("Maps.MapIDs." + map.getID() + ".Crates.CrateIDs." + i + ".Data")) {
					data = (byte) fc.getInt("Maps.MapIDs." + map.getID() + ".Crates.CrateIDs." + i + ".Data");
				}
				
				Crate crate = new Crate(map, i, x, y, z, level, data);
				crates.add(crate);
			}
		}
		return crates;
	}
	
	@SuppressWarnings("deprecation")
	public static void loadCrates(Map m, int percent) {
		for(Crate c : importCrates(Data.MAIN.getFileConfig())) {
			if(c.getMap() == m) {
				c.getMap().getSpawnLocation().getWorld().getBlockAt(c.getX(), c.getY(), c.getZ()).setType(Material.AIR);
			}
		}
		for(Crate c : importCrates(Data.MAIN.getFileConfig())) {
			if(c.getMap() == m) {
				if(c.getLevel() == 0) {
					continue;
				} 
				if(SABTools.randomNumber(0, 100) <= percent) {
					if(c.getLevel() == 1) {
						c.getMap().getSpawnLocation().getWorld().getBlockAt(c.getX(), c.getY(), c.getZ()).setType(Material.CHEST);
						c.getMap().getSpawnLocation().getWorld().getBlockAt(c.getX(), c.getY(), c.getZ()).setData(c.getData());
					}
					if(c.getLevel() == 2) {
						c.getMap().getSpawnLocation().getWorld().getBlockAt(c.getX(), c.getY(), c.getZ()).setType(Material.ENDER_CHEST);
						c.getMap().getSpawnLocation().getWorld().getBlockAt(c.getX(), c.getY(), c.getZ()).setData(c.getData());
					}
				}
			}
		}
	}
}
