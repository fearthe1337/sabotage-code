package com.sabotage.kNoAPP;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class MapVoting implements Listener {
	public static ArrayList<String> lores = new ArrayList<String>();
	public static HashMap<Map, Integer> mapVotes = new HashMap<Map, Integer>();
	public static HashMap<Player, Map> playersVote = new HashMap<Player, Map>();
	
	public static void openVotingInv(Player p) {
		Inventory inv = Bukkit.createInventory(null, 54, "Map Voting");
		int a = 0;
		for(Map map : Map.maps) {
			if(map.getStatus() == false) {
				continue;
			}
			if(!mapVotes.containsKey(map)) {
				mapVotes.put(map, 0);
			}
			
			inv.setItem(a, getMapItem(p, map, a));
			a++;
		}
		
		//Back
		ItemStack is = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte)5);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(ChatColor.GREEN + "<>");
		is.setItemMeta(im);
		for(int i=45; i<54; i++) {
			inv.setItem(i, is);
		}
		
		p.openInventory(inv);
	}
	
	public static ItemStack getMapItem(Player p, Map map, int a) {
		ItemStack is = SABTools.generateDisc(a, map);
		ItemMeta im = is.getItemMeta();
		
		im.setDisplayName(ChatColor.GREEN + map.getName());
		
		if(playersVote.containsKey(p)) {
			if(playersVote.get(p) == map) {
				im.addEnchant(Enchantment.ARROW_INFINITE, 1, true);
				im.addItemFlags(ItemFlag.HIDE_ENCHANTS);
			}
		}
		is.setItemMeta(im);
		return is;
	}
	
	@EventHandler
	public void onInvClick(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked(); // The player that clicked the item
		ItemStack clicked = e.getCurrentItem(); // The item that was clicked
		Inventory inv = e.getInventory(); // The inventory that was clicked in
		
		if(e.getSlotType() == SlotType.CONTAINER) {
			//NPE
			if(clicked.getType() == null || clicked.getType() == Material.AIR || !clicked.hasItemMeta()) {
				return;
			}
			//Valid Item
			if(inv.getName().equals("Map Voting")) {
				e.setCancelled(true);
				int a = 0;
				for(Map map : Map.maps) {
					if(map.getStatus() == false) {
						continue;
					}
					if(getMapItem(p, map, a).isSimilar(clicked)) {
						//Remove Vote
						if(playersVote.containsKey(p)) {
							mapVotes.put(playersVote.get(p), mapVotes.get(playersVote.get(p)) - 1);
							playersVote.remove(p);
						}
						//Cast Vote
						playersVote.put(p, map);
						mapVotes.put(map, mapVotes.get(map) + 1);
						//Finishing Touches
						p.closeInventory();
						p.playSound(p.getLocation(), Sound.BLOCK_NOTE_HARP, 2F, 1F);
						p.sendMessage(ChatColor.GREEN + "Voted for " + ChatColor.DARK_GREEN + map.getName() + "!");
						return;
					}
					a++;
				}
			}
		}
	}
	
	public static Map findPopularMap() {
		int popularVote = 0;
		Map popularMap = Map.maps.get(0);
		for(Map map : mapVotes.keySet()) { //This line may cause errors
			if(map.getStatus() == false) {
				continue;
			}
			int mapsVotes = mapVotes.get(map);
			if(mapsVotes > popularVote) {
				popularVote = mapsVotes;
				popularMap = map;
			}
		}
		return popularMap;
	}
}