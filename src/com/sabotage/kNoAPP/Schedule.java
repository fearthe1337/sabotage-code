package com.sabotage.kNoAPP;

import java.util.Calendar;
import java.util.Date;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.knc.kNoAPP.Rank;

public class Schedule {

	public static Plugin plugin = Bukkit.getPluginManager().getPlugin("Sabotage");
	public static Boolean forceStart = false;
	public static String sabList = "";
	
	public static void runSchedule(int phase, int runs, long delay) {
		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
    		public void run() {
    			if(Sabotage.playersPlaying.size() == 0) {
    				Sabotage.gamePhase = 0;
        			Crate.loadCrates(Map.currentlySelectedMap, 100);
        			IronBar.loadIronBars(Map.currentlySelectedMap, true);
        			Map.currentlySelectedMap = null;
        			MapVoting.mapVotes.clear();
        			MapVoting.playersVote.clear();
        			Role.playersRole.clear();
        			Sabotage.playersWithPasses.clear();
        			Sabotage.devs.clear();
        			Sabotage.timeSinceLastKill.clear();
        			Sabotage.playersAvoidingDetection.clear();
        			Sabotage.playersWithHealthBenefits.clear();
        			Actions.trapChest.clear();
        			Sabotage.detectiveInsight.clear();
        			Sabotage.playersWithBigBoom.clear();
					return;
				}
    			if(phase == 0) {
    				if(runs != -1) {
    					if(runs == Data.MAIN.getFileConfig().getInt("Lobby.Time.MINStartTime")) {
    						if(Sabotage.playersPlaying.size() >= Data.MAIN.getFileConfig().getInt("Lobby.PlayersNeeded") || forceStart == true) {
    							//Empty
    						} else {
    							SABTools.sendGameMessage(ChatColor.RED + "Not enough players! " + ChatColor.DARK_RED + (Data.MAIN.getFileConfig().getInt("Lobby.PlayersNeeded") - Sabotage.playersPlaying.size()) + ChatColor.RED + " more are needed!");
        						SABTools.sendGameSound(Sound.ENTITY_IRONGOLEM_HURT, 2F, 1F);
    							runSchedule(0, Data.MAIN.getFileConfig().getInt("Lobby.Time.MAXStartTime"), delay);
    							return;
    						}
    					}
    					if(runs % 30 == 0 && runs != 0) {
    						SABTools.sendGameMessage(ChatColor.YELLOW + "The game starts in " + ChatColor.GOLD + runs + ChatColor.YELLOW + " seconds!");
    						SABTools.sendGameSound(Sound.BLOCK_NOTE_HARP, 2F, 1F);
    					} else if(runs == 15) {
    						SABTools.sendGameMessage(ChatColor.YELLOW + "The game starts in " + ChatColor.GOLD + runs + ChatColor.YELLOW + " seconds!");
    						SABTools.sendGameSound(Sound.BLOCK_NOTE_HARP, 2F, 1F);
    					} else if(runs == 10) {
    						SABTools.sendGameMessage(ChatColor.YELLOW + "The game starts in " + ChatColor.GOLD + runs + ChatColor.YELLOW + " seconds!");
    						SABTools.sendGameSound(Sound.BLOCK_NOTE_HARP, 2F, 1F);
    					} else if(runs == 5) {
    						SABTools.sendGameMessage(ChatColor.YELLOW + "The game starts in " + ChatColor.GOLD + runs + ChatColor.YELLOW + " seconds!");
    						SABTools.sendGameSound(Sound.BLOCK_NOTE_HARP, 2F, 1F);
    					} else if(runs == 4) {
    						SABTools.sendGameMessage(ChatColor.YELLOW + "The game starts in " + ChatColor.GOLD + runs + ChatColor.YELLOW + " seconds!");
    						SABTools.sendGameSound(Sound.BLOCK_NOTE_HARP, 2F, 0.9F);
    					} else if(runs == 3) {
    						SABTools.sendGameMessage(ChatColor.YELLOW + "The game starts in " + ChatColor.GOLD + runs + ChatColor.YELLOW + " seconds!");
    						SABTools.sendGameSound(Sound.BLOCK_NOTE_HARP, 2F, 0.8F);
    					} else if(runs == 2) {
    						SABTools.sendGameMessage(ChatColor.YELLOW + "The game starts in " + ChatColor.GOLD + runs + ChatColor.YELLOW + " seconds!");
    						SABTools.sendGameSound(Sound.BLOCK_NOTE_HARP, 2F, 0.7F);
    					} else if(runs == 1) {
    						SABTools.sendGameMessage(ChatColor.YELLOW + "The game starts in " + ChatColor.GOLD + runs + ChatColor.YELLOW + " second!");
    						SABTools.sendGameSound(Sound.BLOCK_NOTE_HARP, 2F, 0.6F);
    					} else if(runs == 0) {
    						SABTools.sendGameMessage(ChatColor.YELLOW + "The game starts now!");
    						SABTools.sendGameSound(Sound.BLOCK_NOTE_HARP, 2F, 0.5F);
    					}
    					runSchedule(0, runs - 1, delay);
    				} else {
						forceStart = false;
						if(Sabotage.playersPlaying.size() < 3 && Sabotage.devs.size() == 0) {
							SABTools.sendGameMessage(ChatColor.DARK_RED + "An absolute minimum of 3 players is required. Sorry!");
							SABTools.sendGameSound(Sound.ENTITY_ENDERDRAGON_GROWL, 2F, 1F);
							runSchedule(0, Data.MAIN.getFileConfig().getInt("Lobby.Time.MAXStartTime"), delay);
							return;
						} else {
							Map winner = MapVoting.findPopularMap();
							//Check Fields
							if(winner.getSpawnLocation() == null) {
								Map.refreshMaps(false);
								winner = MapVoting.findPopularMap();
								if(winner.getSpawnLocation() == null) {
									SABTools.sendGameMessage(ChatColor.RED + "Map " + ChatColor.DARK_RED + winner.getName() + ChatColor.RED + " is missing a spawn location!");
									SABTools.sendGameSound(Sound.ENTITY_ENDERDRAGON_GROWL, 2F, 1F);
									runSchedule(0, Data.MAIN.getFileConfig().getInt("Lobby.Time.MAXStartTime"), delay);
									return;
								}
							}
							Map.currentlySelectedMap = winner;
							for(Player pl : Sabotage.playersPlaying) {
								Karma.updateKarma(pl);
								pl.teleport(winner.getSpawnLocation());
								SABTools.sendGameSound(Sound.ENTITY_PLAYER_LEVELUP, 2F, 1F);
								pl.setInvulnerable(true);
								pl.setCanPickupItems(true);
								pl.getInventory().clear();
								pl.sendMessage(ChatColor.GOLD + "Your role will be announce in " + Data.MAIN.getFileConfig().getInt("General.GameOptions.SafeRunningTime") + " seconds!");
								pl.sendMessage(ChatColor.GRAY + "Find crates around the map and prepare for the Sabotage!");
								pl.getInventory().addItem(ItemManagement.getInspectorItem());
							}
							Crate.refreshCrates();
							Crate.loadCrates(winner, Data.MAIN.getFileConfig().getInt("World.Crates.SpawnPercentEachRound"));
							StatusWool.refreshSWs();
							StatusWool.loadSW(winner);
							IronBar.refreshIronBars();
							IronBar.loadIronBars(winner, true);
							TesterSign.refreshTesterSigns();
							Sabotage.gamePhase = 1;
							runSchedule(1, Data.MAIN.getFileConfig().getInt("General.GameOptions.SafeRunningTime"), delay);
							return;
						}
    				} 
    			}
    			if(phase == 1) {
    				if(runs % 15 == 0 && runs != 0 && runs != Data.MAIN.getFileConfig().getInt("General.GameOptions.SafeRunningTime")) {
    					SABTools.sendGameMessage(ChatColor.GREEN + "Roles will be announced in " + ChatColor.GRAY + runs + ChatColor.GREEN + " seconds!");
						SABTools.sendGameSound(Sound.BLOCK_NOTE_HARP, 2F, 1F);
    				} else if(runs == 10) {
    					SABTools.sendGameMessage(ChatColor.GREEN + "Roles will be announced in " + ChatColor.GRAY + runs + ChatColor.GREEN + " seconds!");
						SABTools.sendGameSound(Sound.BLOCK_NOTE_HARP, 2F, 1F);
					} else if(runs == 5) {
						SABTools.sendGameMessage(ChatColor.GREEN + "Roles will be announced in " + ChatColor.GRAY + runs + ChatColor.GREEN + " seconds!");
						SABTools.sendGameSound(Sound.BLOCK_NOTE_HARP, 2F, 1F);
					} else if(runs == 4) {
						SABTools.sendGameMessage(ChatColor.GREEN + "Roles will be announced in " + ChatColor.GRAY + runs + ChatColor.GREEN + " seconds!");
						SABTools.sendGameSound(Sound.BLOCK_NOTE_HARP, 2F, 0.9F);
					} else if(runs == 3) {
						SABTools.sendGameMessage(ChatColor.GREEN + "Roles will be announced in " + ChatColor.GRAY + runs + ChatColor.GREEN + " seconds!");
						SABTools.sendGameSound(Sound.BLOCK_NOTE_HARP, 2F, 0.8F);
					} else if(runs == 2) {
						SABTools.sendGameMessage(ChatColor.GREEN + "Roles will be announced in " + ChatColor.GRAY + runs + ChatColor.GREEN + " seconds!");
						SABTools.sendGameSound(Sound.BLOCK_NOTE_HARP, 2F, 0.7F);
					} else if(runs == 1) {
						SABTools.sendGameMessage(ChatColor.GREEN + "Roles will be announced in " + ChatColor.GRAY + runs + ChatColor.GREEN + " second!");
						SABTools.sendGameSound(Sound.BLOCK_NOTE_HARP, 2F, 0.6F);
					} else if(runs == 0) {
						IronBar.loadIronBars(Map.currentlySelectedMap, false);
						Role.generateRoles();
						for(Player pl : Sabotage.playersPlaying) {
							pl.setScoreboard(Role.playersRole.get(pl).generateScoreboard(pl));
							pl.sendMessage(ChatColor.GOLD + "There are " + ChatColor.BLUE + Sabotage.playersPlaying.size() + ChatColor.GOLD + " players in this game.");
							pl.sendMessage(ChatColor.GOLD + "Good Luck!");
							if(Role.playersRole.get(pl) == Role.INNOCENT) {
								pl.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "You are a " + Role.INNOCENT.getColor() + "" + ChatColor.BOLD + Role.INNOCENT.getName() + ChatColor.GOLD + "" + ChatColor.BOLD + " this game!");
								pl.sendMessage(ChatColor.BLUE + "You can use your magnifier (glass bottle) to investigate *living* players!");
								pl.sendMessage(ChatColor.GOLD + "Your job is to find and kill the " + Role.SABOTEUR.getColor() + Role.SABOTEUR.getName() + "s" + ChatColor.GOLD + ".");
							}
							if(Role.playersRole.get(pl) == Role.DETECTIVE) {
								pl.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "You are the " + Role.DETECTIVE.getColor() + "" + ChatColor.BOLD + Role.DETECTIVE.getName() + ChatColor.GOLD + "" + ChatColor.BOLD + " this game!");
								pl.sendMessage(ChatColor.BLUE + "Your job is to lead the investigation against the " + Role.SABOTEUR.getColor() + Role.SABOTEUR.getName() + "s" + ChatColor.BLUE + ".");
							}
							if(Role.playersRole.get(pl) == Role.SABOTEUR) {
								pl.sendMessage(ChatColor.GOLD + "" + ChatColor.BOLD + "You are a " + Role.SABOTEUR.getColor() + "" + ChatColor.BOLD + Role.SABOTEUR.getName() + ChatColor.GOLD + "" + ChatColor.BOLD + " this game!");
								if(Sabotage.playersWithPasses.contains(pl)) {
									Passes.removeSabPasses(pl.getName(), 1);
								}
								String sabs = "";
								for(Player pla : Sabotage.playersPlaying) {
									if(Role.playersRole.get(pla) == Role.SABOTEUR) {
										if(sabs.equals("")) {
											sabs = sabs + pla.getName();
										} else {
											sabs = sabs + ", " + pla.getName();
										}
									}
								}
								sabList = sabs;
								pl.sendMessage(ChatColor.GOLD + "Your fellow saboteurs are: " + ChatColor.RED + sabs);
								pl.sendMessage(ChatColor.GOLD + "Your job is to kill all the " + Role.INNOCENT.getColor() + "innocent" + ChatColor.GOLD + " players and the " + Role.DETECTIVE.getColor() + Role.DETECTIVE.getName() + ChatColor.GOLD + ".");
							}
							for(Player pla : Sabotage.playersPlaying) {
								if(pla != pl && Role.playersRole.get(pla) == Role.DETECTIVE) {
									pl.sendMessage(ChatColor.GOLD + "The detective is " + Role.DETECTIVE.getColor() + pla.getName());
								}
							}
						}
						Sabotage.gamePhase = 2;
						for(Player pl : Sabotage.playersPlaying) {
							pl.setCanPickupItems(true);
							pl.setInvulnerable(false);
							pl.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 20, 20));
							pl.setHealth(20);
							pl.setLevel(Karma.getKarma(pl.getName()));
						}
						runSchedule(2, 0, delay);
						return;
					}
    				runSchedule(1, runs - 1, delay);
    			}
    			if(phase == 2) {
    				int aliveInno = 0;
    				int aliveDetect = 0;
    				int aliveSab = 0;
    				if(runs != 0 && Data.MAIN.getFileConfig().getInt("World.Crates.RespawnEveryXSeconds") != 0) {
    					if(runs % Data.MAIN.getFileConfig().getInt("World.Crates.RespawnEveryXSeconds") == 0) {
    						Crate.loadCrates(Map.currentlySelectedMap, Data.MAIN.getFileConfig().getInt("World.Crates.SpawnPercentEachRound"));
    						SABTools.sendGameMessage(ChatColor.GOLD + "Chests have respawned around the map!");
    						SABTools.sendGameSound(Sound.ENTITY_FIREWORK_LAUNCH, 2F, 1F);
    					}
    				}
    				for(Player pl : Sabotage.playersPlaying) {
    					if(Sabotage.timeSinceLastKill.containsKey(pl)) {
    						if(Sabotage.timeSinceLastKill.get(pl) >= 60) {
    							Sabotage.timeSinceLastKill.remove(pl);
    						} else {
    							Sabotage.timeSinceLastKill.put(pl, Sabotage.timeSinceLastKill.get(pl) + 1);
    						}
    					}
        					
    					if(pl.getGameMode() == GameMode.SPECTATOR && pl.getLocation().distance(Map.currentlySelectedMap.getSpawnLocation()) > Data.MAIN.getFileConfig().getInt("World.SpectatorMapRange")) {
    						pl.teleport(Map.currentlySelectedMap.getSpawnLocation());
    						pl.sendMessage(ChatColor.RED + "You've traveled too far away from the action!");
    						pl.playSound(pl.getLocation(), Sound.ENTITY_CHICKEN_HURT, 2F, 1F);
    					}
    					
    					if(pl.getGameMode() != GameMode.SPECTATOR) {
    						if(Role.playersRole.get(pl) == Role.INNOCENT) {
    							aliveInno++;
    						}
    						if(Role.playersRole.get(pl) == Role.DETECTIVE) {
    							aliveDetect++;
    						}
    						if(Role.playersRole.get(pl) == Role.SABOTEUR) {
    							aliveSab++;
    						}
    					}
    				}
    				if(Sabotage.devs.size() == 0 || runs > 30) {
    					if(((aliveInno == 0 && aliveDetect == 0) || aliveSab == 0)) {
    						Sabotage.gamePhase = 3;
    						endGame(aliveInno, aliveDetect, aliveSab, delay);
    						return;
    					}
    				}
    				runSchedule(2, runs + 1, delay);
    			}
    		}
		}, delay);
	}
	
	public static void endGame(int aliveInno, int aliveDetect, int aliveSab, long delay) {
		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
    		public void run() {
    			SABTools.sendGameSound(Sound.ENTITY_ENDERDRAGON_SHOOT, 5F, 0.5F);
    			if(aliveSab == 0) {
    				SABTools.sendGameMessage(Role.INNOCENT.getColor() + "The " + Role.INNOCENT.getName() + " players win!");
    				SABTools.sendGameMessage(ChatColor.GOLD + "The saboteurs were: ");
    				SABTools.sendGameMessage(ChatColor.RED + sabList);
    				if(Data.MAIN.getFileConfig().getInt("Karma.Values.Innocent.WinGame") > 0) {
    					SABTools.sendGameMessage(ChatColor.GOLD + "All alive innocents got a Karma bonus of " + Data.MAIN.getFileConfig().getInt("Karma.Values.Innocent.WinGame") + " Karma!");
    				} else if(Data.MAIN.getFileConfig().getInt("Karma.Values.Innocent.WinGame") < 0) {
    					SABTools.sendGameMessage(ChatColor.GOLD + "All alive innocents got a Karma penalty of " + (-1 * Data.MAIN.getFileConfig().getInt("Karma.Values.Innocent.WinGame")) + " Karma!");
    				} else {
    					SABTools.sendGameMessage(ChatColor.GRAY + "Alive innocent's Karma was uneffected.");
    				}
    			
    				if(Data.MAIN.getFileConfig().getInt("Karma.Values.Detective.WinGame") > 0) {
    					SABTools.sendGameMessage(ChatColor.BLUE + "The Detective got a Karma bonus of " + Data.MAIN.getFileConfig().getInt("Karma.Values.Detective.WinGame") + " Karma!");
    				} else if(Data.MAIN.getFileConfig().getInt("Karma.Values.Detective.WinGame") < 0) {
    					SABTools.sendGameMessage(ChatColor.BLUE + "The Detective got a Karma penalty of " + Data.MAIN.getFileConfig().getInt("Karma.Values.Detective.WinGame") + " Karma!");
    				} else {
    					SABTools.sendGameMessage(ChatColor.BLUE + "The Detective's Karma was uneffected.");
    				}
    			
    				for(Player pl : Sabotage.playersPlaying) {
    					if(pl.getGameMode() == GameMode.SPECTATOR) {
    						continue;
    					}
    					if(Role.playersRole.get(pl) == Role.INNOCENT) {
    						Karma.addKarma(pl.getName(), Data.MAIN.getFileConfig().getInt("Karma.Values.Innocent.WinGame"));
    					}
    					if(Role.playersRole.get(pl) == Role.DETECTIVE) {
    						Karma.addKarma(pl.getName(), Data.MAIN.getFileConfig().getInt("Karma.Values.Detective.WinGame"));
    					}
    					if(Role.playersRole.get(pl) == Role.SABOTEUR) {
    						Karma.addKarma(pl.getName(), Data.MAIN.getFileConfig().getInt("Karma.Values.Saboteur.LoseGame"));
    					}
    					Karma.updateKarma(pl);    				
    				}
    			} else if(aliveInno == 0 && aliveDetect == 0) {
    				SABTools.sendGameMessage(Role.SABOTEUR.getColor() + "The " + Role.SABOTEUR.getName() + " players win!");
    				SABTools.sendGameMessage(ChatColor.GOLD + "The saboteurs were: ");
    				SABTools.sendGameMessage(ChatColor.RED + sabList);
    				if(Data.MAIN.getFileConfig().getInt("Karma.Values.Saboteur.WinGame") > 0) {
    					SABTools.sendGameMessage(ChatColor.GOLD + "All alive Saboteurs got a Karma bonus of " + Data.MAIN.getFileConfig().getInt("Karma.Values.Saboteur.WinGame") + " Karma!");
    				} else if(Data.MAIN.getFileConfig().getInt("Karma.Values.Saboteur.WinGame") < 0) {
    					SABTools.sendGameMessage(ChatColor.GOLD + "All alive Saboteurs got a Karma penalty of " + (-1 * Data.MAIN.getFileConfig().getInt("Karma.Values.Saboteur.WinGame")) + " Karma!");
    				} else {
    					SABTools.sendGameMessage(ChatColor.GRAY + "Alive Saboteurs' Karma was uneffected.");
    				}
    			
    				for(Player pl : Sabotage.playersPlaying) {
    					if(pl.getGameMode() == GameMode.SPECTATOR) {
    						continue;
    					}
    					if(Role.playersRole.get(pl) == Role.INNOCENT) {
    						Karma.addKarma(pl.getName(), Data.MAIN.getFileConfig().getInt("Karma.Values.Innocent.LoseGame"));
    					}
    					if(Role.playersRole.get(pl) == Role.DETECTIVE) {
    						Karma.addKarma(pl.getName(), Data.MAIN.getFileConfig().getInt("Karma.Values.Detective.LoseGame"));
    					}
    					if(Role.playersRole.get(pl) == Role.SABOTEUR) {
    						Karma.addKarma(pl.getName(), Data.MAIN.getFileConfig().getInt("Karma.Values.Saboteur.WinGame"));
    					}
    					Karma.updateKarma(pl);
    				}
    			}
    			endGamePartTwo(delay);
    		}
		}, 10L);
	}
	
	public static void endGamePartTwo(long delay) {
		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
    		public void run() {
    			if(Data.MAIN.getFileConfig().getBoolean("General.RestartServerOnFinish") == true) {
    				for(Player pl : Bukkit.getOnlinePlayers()) {
    					SABTools.onLeave(pl, false);
    					pl.kickPlayer(ChatColor.RED + "" + ChatColor.BOLD + "Sabotage " + ChatColor.WHITE + "" + ChatColor.BOLD + "is restarting the server...");
    				}
    				Bukkit.shutdown();
    				return;
    			}
    			for(Player pl : Sabotage.playersPlaying) {
    				for(PotionEffect pe : pl.getActivePotionEffects()) {
    					pl.removePotionEffect(pe.getType());
    				}	
    				pl.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
    				pl.setGameMode(GameMode.SURVIVAL);
    				pl.setHealth(20);
    				pl.teleport((Location) Data.MAIN.getFileConfig().get("World.Locations.Lobby"));
    				pl.playSound(pl.getLocation(), Sound.ENTITY_ENDERDRAGON_GROWL, 2F, 1F);
    				SABTools.clearFullInv(pl);
    				pl.getInventory().setItem(0, ItemManagement.getMapSelector());
    				pl.getInventory().setItem(4, ItemManagement.getKarmaCount(pl));
    				pl.setInvulnerable(true);
    				pl.setCanPickupItems(false);
    				if(Bukkit.getPluginManager().getPlugin("kNoCore") != null) {
    					if(Rank.inheritsRank(pl, Rank.SILVER)) {
    						if(Passes.getSabPasses(pl.getName()) < 1) {
    							Passes.addSabPasses(pl.getName(), 1 - Passes.getSabPasses(pl.getName()));
    						}
    					}
    				}
    				if(Data.MAIN.getFileConfig().getInt("Player." + pl.getUniqueId() + ".SaboteurPasses") > 0) {
    					if(Sabotage.playersWithPasses.contains(pl)) {
    						pl.getInventory().setItem(8, ItemManagement.getSabPass(true, Data.PLAYER.getFileConfig().getInt("Player." + pl.getUniqueId() + ".SaboteurPasses")));
    					} else {
    						pl.getInventory().setItem(8, ItemManagement.getSabPass(false, Data.PLAYER.getFileConfig().getInt("Player." + pl.getUniqueId() + ".SaboteurPasses")));
    					}
    				} else {
    					pl.getInventory().setItem(8, ItemManagement.getNoSabPass());
    				}
    				if(Sabotage.devs.contains(pl)) {
    					pl.sendMessage(ChatColor.DARK_RED + "The round has ended. You were removed from Dev Mode!");
    				}
    			}
    			Sabotage.gamePhase = 0;
    			Crate.loadCrates(Map.currentlySelectedMap, 100);
    			IronBar.loadIronBars(Map.currentlySelectedMap, true);
    			Map.currentlySelectedMap = null;
    			MapVoting.mapVotes.clear();
    			MapVoting.playersVote.clear();
    			Role.playersRole.clear();
    			Sabotage.playersWithPasses.clear();
    			Sabotage.devs.clear();
    			Sabotage.timeSinceLastKill.clear();
    			Sabotage.playersAvoidingDetection.clear();
    			Sabotage.playersWithHealthBenefits.clear();
    			Actions.trapChest.clear();
    			Sabotage.detectiveInsight.clear();
    			Sabotage.playersWithBigBoom.clear();
    			runSchedule(0, Data.MAIN.getFileConfig().getInt("Lobby.Time.MAXStartTime"), delay);
    		}
		}, 200L);
	}
	
	public static void runChecks() {
		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
    		public void run() {
    			Date date= new Date();
				Calendar cal = Calendar.getInstance();
				cal.setTime(date);
				int min = cal.get(Calendar.MINUTE); //Starts 0
				int hour = cal.get(Calendar.HOUR_OF_DAY); //Starts 0 (One hour behind Greenwood)
				
				if(hour == 0 && min == 0) {
					for(Player p : Bukkit.getOnlinePlayers()) {
						Karma.addKarma(p.getName(), Data.MAIN.getFileConfig().getInt("Karma.DailyReward"));
					}
					for(OfflinePlayer op : Bukkit.getOfflinePlayers()) {
						Karma.addKarma(op.getName(), Data.MAIN.getFileConfig().getInt("Karma.DailyReward"));
					}
				}
				runChecks();
    		}
		}, 600L);
	}
}
