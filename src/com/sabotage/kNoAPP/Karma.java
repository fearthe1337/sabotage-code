package com.sabotage.kNoAPP;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class Karma {

	@SuppressWarnings("deprecation")
	public static int getKarma(String name) {
		Player tON = Bukkit.getPlayerExact(name);
		if(tON == null) {
			OfflinePlayer tOFF = Bukkit.getOfflinePlayer(name);
			return Data.PLAYER.getFileConfig().getInt("Player." + tOFF.getUniqueId() + ".Karma");
		} else {
			return Data.PLAYER.getFileConfig().getInt("Player." + tON.getUniqueId() + ".Karma");
		}
	}
	@SuppressWarnings("deprecation")
	public static void addKarma(String name, int amt) {
		FileConfiguration fc = Data.PLAYER.getFileConfig();
		Player tON = Bukkit.getPlayerExact(name);
		if(tON == null) {
			OfflinePlayer tOFF = Bukkit.getOfflinePlayer(name);
			fc.set("Player." + tOFF.getUniqueId()+ ".Karma", fc.getInt("Player." + tOFF.getUniqueId() + ".Karma") + amt);
		} else {
			fc.set("Player." + tON.getUniqueId()+ ".Karma", fc.getInt("Player." + tON.getUniqueId() + ".Karma") + amt);
			if(Sabotage.playersPlaying.contains(tON)) {
				updateKarma(tON);
			}
		}
		Data.PLAYER.saveDataFile(fc);
	}
	
	@SuppressWarnings("deprecation")
	public static void removeKarma(String name, int amt) {
		FileConfiguration fc = Data.PLAYER.getFileConfig();
		Player tON = Bukkit.getPlayerExact(name);
		if(tON == null) {
			OfflinePlayer tOFF = Bukkit.getOfflinePlayer(name);
			fc.set("Player." + tOFF.getUniqueId()+ ".Karma", fc.getInt("Player." + tOFF.getUniqueId() + ".Karma") - amt);
		} else {
			fc.set("Player." + tON.getUniqueId()+ ".Karma", fc.getInt("Player." + tON.getUniqueId() + ".Karma") - amt);
			if(Sabotage.playersPlaying.contains(tON)) {
				updateKarma(tON);
			}
		}
		Data.PLAYER.saveDataFile(fc);
	}
	
	public static void updateKarma(Player p) {
		p.setLevel(Karma.getKarma(p.getName()));
		if(Data.MAIN.getFileConfig().getBoolean("General.BanOnZeroKarma") == true) {
			if(Karma.getKarma(p.getName()) <= 0) {
				Sabotage.playersPlaying.remove(p);
				Karma.addKarma(p.getName(), Karma.getKarma(p.getName()) * -1);
				Sabotage.playersPlaying.add(p);
				p.sendMessage(ChatColor.DARK_RED + "You have run out of Karma! Come back tomorrow for more!");
				p.playSound(p.getLocation(), Sound.ENTITY_WITHER_SHOOT, 2F, 0.5F);
				SABTools.onLeave(p, false);
			}
		}
	}
}
