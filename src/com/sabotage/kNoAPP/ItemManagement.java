package com.sabotage.kNoAPP;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

public class ItemManagement {
	public static Plugin plugin = Bukkit.getPluginManager().getPlugin("Sabotage");
	
	public static HashMap<Player, Integer> inspecterCooldown = new HashMap<Player, Integer>();

	public static ItemStack getMapSelector() {
		ItemStack is = new ItemStack(Material.GREEN_RECORD, 1);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(ChatColor.AQUA + "Vote for a Map");
		ArrayList<String> lores = new ArrayList<String>();
		lores.add(ChatColor.YELLOW + "Select a map to play!");
		im.setLore(lores);
		is.setItemMeta(im);
		return is;
	}
	
	public static ItemStack getKarmaCount(Player p) {
		ItemStack is = new ItemStack(Material.DOUBLE_PLANT, 1);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Karma" + ChatColor.WHITE + ": " + ChatColor.GRAY + Data.PLAYER.getFileConfig().getInt("Player." + p.getUniqueId() + ".Karma"));
		ArrayList<String> lores = new ArrayList<String>();
		lores.add(ChatColor.GRAY + "Your total Karma in Sabotage.");
		im.setLore(lores);
		is.setItemMeta(im);
		return is;
	}
	
	public static ItemStack getNoSabPass() {
		ItemStack is = new ItemStack(Material.INK_SACK, 1, (byte) 8);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(ChatColor.RED + "Owned Saboteur Passes");
		ArrayList<String> lores = new ArrayList<String>();
		lores.add(ChatColor.YELLOW + "Get priority as a Saboteur!");
		im.setLore(lores);
		is.setItemMeta(im);
		return is;
	}
	
	public static ItemStack getSabPass(Boolean active, int amount) {
		ItemStack is = new ItemStack(Material.NAME_TAG, amount);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(ChatColor.GREEN + "Owned Saboteur Passes");
		ArrayList<String> lores = new ArrayList<String>();
		lores.add(ChatColor.YELLOW + "Get priority as a Saboteur!");
		im.setLore(lores);
		if(active == true) {
			im.addEnchant(Enchantment.ARROW_INFINITE, 1, false);
			im.addItemFlags(ItemFlag.HIDE_ENCHANTS);
		}
		is.setItemMeta(im);
		return is;
	}
	
	public static ItemStack getCrateItem(Map map, int level) {
		ItemStack is = null;
		ItemMeta im = null;
		if(level == 1) {
			is = new ItemStack(Material.CHEST, 1);
			im = is.getItemMeta();
			im.setDisplayName(ChatColor.YELLOW + "Level One Chest");
		}
		if(level == 2) {
			is = new ItemStack(Material.ENDER_CHEST, 1);
			im = is.getItemMeta();
			im.setDisplayName(ChatColor.RED + "Level Two Chest");
		}
		ArrayList<String> lores = new ArrayList<String>();
		lores.add(ChatColor.GRAY + "Map Data");
		lores.add(ChatColor.DARK_PURPLE + map.getName());
		im.setLore(lores);
		is.setItemMeta(im);
		return is;
	}
	
	public static ItemStack getSignItem(Map map) {
		ItemStack is = new ItemStack(Material.SIGN, 1);
		ItemMeta im = is.getItemMeta();
		ArrayList<String> lores = new ArrayList<String>();
		lores.add(ChatColor.GRAY + "Clicking this sign begins Saboteur Test");
		lores.add(ChatColor.DARK_PURPLE + map.getName());
		im.setLore(lores);
		im.setDisplayName(ChatColor.YELLOW + "Begin Saboteur Test Sign");
		is.setItemMeta(im);
		return is;
	}
	
	@SuppressWarnings("deprecation")
	public static ItemStack getBarsItem(Map map) {
		ItemStack is = new ItemStack(Material.getMaterial(101), 1);
		ItemMeta im = is.getItemMeta();
		ArrayList<String> lores = new ArrayList<String>();
		lores.add(ChatColor.GRAY + "Bars appear during Saboteur Test");
		lores.add(ChatColor.DARK_PURPLE + map.getName());
		im.setLore(lores);
		im.setDisplayName(ChatColor.YELLOW + "Saboteur Block Bars");
		is.setItemMeta(im);
		return is;
	}
	
	public static ItemStack getWoolItem(Map map) {
		ItemStack is = new ItemStack(Material.WOOL, 1);
		ItemMeta im = is.getItemMeta();
		ArrayList<String> lores = new ArrayList<String>();
		lores.add(ChatColor.GRAY + "Wool Indicates Result");
		lores.add(ChatColor.DARK_PURPLE + map.getName());
		im.setLore(lores);
		im.setDisplayName(ChatColor.YELLOW + "Result Indicator");
		is.setItemMeta(im);
		return is;
	}
	
	public static ItemStack getInspectorItem() {
		ItemStack is = new ItemStack(Material.SHEARS, 1);
		ItemMeta im = is.getItemMeta();
		ArrayList<String> lores = new ArrayList<String>();
		lores.add(ChatColor.GRAY + "Use this item to inspect");
		lores.add(ChatColor.GRAY + "other players!");
		im.setLore(lores);
		im.setDisplayName(ChatColor.YELLOW + "Inspection Shears");
		is.setItemMeta(im);
		return is;
	}
	
	public static void runInspectorItemCooldown(Player p) {
		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
    		public void run() {
    			if(inspecterCooldown.get(p) != 0) {
    				inspecterCooldown.put(p, inspecterCooldown.get(p) - 1);
    				runInspectorItemCooldown(p);
    			} else {
    				inspecterCooldown.remove(p);
    				return;
    			}
    		}
		}, 20L);
	}
	
	public static ItemStack getCompassItem() {
		ItemStack is = new ItemStack(Material.COMPASS, 1);
		ItemMeta im = is.getItemMeta();
		ArrayList<String> lores = new ArrayList<String>();
		lores.add(ChatColor.GRAY + "Right Click to Refresh!");
		im.setLore(lores);
		im.setDisplayName(ChatColor.YELLOW + "Tracking Nearest Player...");
		is.setItemMeta(im);
		return is;
	}
	
	public static ItemStack getSurpriseItem() {
		ItemStack is = new ItemStack(Material.CHEST, 1);
		ItemMeta im = is.getItemMeta();
		ArrayList<String> lores = new ArrayList<String>();
		lores.add(ChatColor.GRAY + "Explodes when opened.");
		im.setLore(lores);
		im.setDisplayName(ChatColor.RED + "Surprise Chest");
		is.setItemMeta(im);
		return is;
	}
}
