package com.sabotage.kNoAPP;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.block.DoubleChest;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.BlockSpreadEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerGameModeChangeEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import com.knc.kNoAPP.Cosmetic;
import com.knc.kNoAPP.Rank;

public class Sabotage extends JavaPlugin implements Listener {
	
	public static ArrayList<Player> playersPlaying = new ArrayList<Player>();
	public static ArrayList<Player> playersWithPasses = new ArrayList<Player>();
	public static ArrayList<Player> devs = new ArrayList<Player>();
	public static HashMap<Player, ItemStack[]> savedInventory = new HashMap<Player, ItemStack[]>();
	public static HashMap<Player, Location> savedLocation = new HashMap<Player, Location>();
	public static HashMap<Player, GameMode> savedGamemode = new HashMap<Player, GameMode>();
	public static HashMap<Player, ItemStack[]> savedArmorInventory = new HashMap<Player, ItemStack[]>();
	public static HashMap<Player, Integer> savedTotalExperiance = new HashMap<Player, Integer>();
	
	public static ArrayList<Player> playersAvoidingDetection = new ArrayList<Player>();
	public static ArrayList<Player> playersWithHealthBenefits = new ArrayList<Player>();
	public static ArrayList<Player> detectiveInsight = new ArrayList<Player>();
	public static ArrayList<Player> playersWithBigBoom = new ArrayList<Player>();
	public static HashMap<Player, Integer> timeSinceLastKill = new HashMap<Player, Integer>();
	
	public static boolean knc;
	
	public static int gamePhase;
	
	@Override
	public void onEnable() {
		getLogger().info("Starting up...");
		this.getServer().getPluginManager().registerEvents(this, this);
		this.getServer().getPluginManager().registerEvents(new MapVoting(), this);
		this.getServer().getPluginManager().registerEvents(new Actions(), this);
		
		getLogger().info("Loading Configuration Files...");
		for(Data d : Data.configs) {
			if(d != Data.CONFIG) {
				if(Data.CONFIG.getFileConfig().getBoolean("UseMainFolder") == true) {
					d.setFile("");
				} else {
					d.setFile(Data.CONFIG.getFileConfig().getString("UseCustomFolder"));
				}
			}
			d.createDataFile();
		}
		gamePhase = 0;
		Map.currentlySelectedMap = null;
		getLogger().info("Importing Maps...");
		Map.refreshMaps(true);
		getLogger().info("Importing Crates...");
		Crate.refreshCrates();
		getLogger().info("Importing Bars...");
		IronBar.refreshIronBars();
		getLogger().info("Importing Wool...");
		StatusWool.refreshSWs();
		getLogger().info("Importing Signs...");
		TesterSign.refreshTesterSigns();
		getLogger().info("Finishing up...");
		TesterSign.inUse = false;
		Loot.loadLoot();
		ShopItem.loadShopItems();
		
		Schedule.runChecks();
		
		if(Bukkit.getPluginManager().getPlugin("kNoCore") != null) {
			knc = true;
		} else {
			knc = false;
		}
		getLogger().info("We good!");
	}
	
	@Override
	public void onDisable() {
		if(gamePhase != 0) {
			Sabotage.gamePhase = 0;
			Crate.loadCrates(Map.currentlySelectedMap, 100);
			IronBar.loadIronBars(Map.currentlySelectedMap, true);
		}
		for(Player pl : Bukkit.getOnlinePlayers()) {
			SABTools.onLeave(pl, true);
			pl.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
		}
		getLogger().info("Till next time!");
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(sender instanceof Player) {
			Player p = (Player) sender;
			if(cmd.getName().equalsIgnoreCase("sab")) {
				if(args.length == 0) {
					p.sendMessage(ChatColor.GREEN + "Sabotage " + ChatColor.WHITE + "- By kNoAPP");
					p.sendMessage(ChatColor.GRAY + "/sab help for Sabotage Commands");
					return true;
				}
				if(args.length == 1) {
					if(args[0].equalsIgnoreCase("help")) {
						if(p.hasPermission("sabotage.help")) {
							p.sendMessage(ChatColor.GOLD + "\nSabotage by kNoAPP " + ChatColor.YELLOW + "Command List (1/3)");
							p.sendMessage(ChatColor.DARK_GREEN + "-----------------------------------------------");
							p.sendMessage(ChatColor.GRAY + "  /sab join " + ChatColor.WHITE + "- Join the Game");
							p.sendMessage(ChatColor.GRAY + "  /sab leave " + ChatColor.WHITE + "- Leave the Game");
							p.sendMessage(ChatColor.GRAY + "  /sab shop " + ChatColor.WHITE + "- In-Game Shop");
							p.sendMessage(ChatColor.GRAY + "  /sab setLobby " + ChatColor.WHITE + "- Set Sabotage Lobby");
							p.sendMessage(ChatColor.GRAY + "  /sab create <mapname> " + ChatColor.WHITE + "- Create a Map");
							p.sendMessage(ChatColor.GRAY + "  /sab remove <mapname> " + ChatColor.WHITE + "- Remove a Map");
							p.sendMessage(ChatColor.DARK_GREEN + "-----------------------------------------------");
							return true;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.help");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("join")) {
						if(p.hasPermission("sabotage.play")) {
							SABTools.onJoin(p);
							return true;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.play");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("leave")) {
						if(p.hasPermission("sabotage.play")) {
							SABTools.onLeave(p, false);
							return true;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.play");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("shop")) {
						if(p.hasPermission("sabotage.shop")) {
							if(playersPlaying.contains(p) && gamePhase == 2) {
								p.sendMessage(ChatColor.DARK_RED + "/sab shop " + ChatColor.RED + "Buy with /sab buy <ID>");
								p.sendMessage(ChatColor.DARK_GREEN + "-----------------------------------------------");
								int id = 0;
								for(ShopItem si : ShopItem.shopItems) {
									if(si.getApplicableRoles().contains(Role.playersRole.get(p))) {
										id++;
										p.sendMessage(ChatColor.GREEN + "" + id + "- " + ChatColor.GRAY + si.getName() + " (" + si.getCost() + ") "+ ChatColor.WHITE + si.getDesc());
									}
								}
								p.sendMessage(ChatColor.DARK_GREEN + "-----------------------------------------------");
								return true;
							}
							p.sendMessage(ChatColor.RED + "You cannot use this right now!");
							return false;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.shop");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("buy")) {
						if(p.hasPermission("sabotage.shop")) {
							if(playersPlaying.contains(p) && gamePhase == 2) {
								p.sendMessage(ChatColor.RED + "Invalid Args! " + ChatColor.DARK_RED + "/sab buy <ID>");
								return false;
							}
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.shop");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("setlobby")) {
						if(p.hasPermission("sabotage.map.setlobby")) {
							FileConfiguration fc = Data.MAIN.getFileConfig();
							fc.set("World.Locations.Lobby", p.getLocation());
							Data.MAIN.saveDataFile(fc);
							p.sendMessage(ChatColor.GREEN + "Sabotage Lobby has been added as your Location!");
							return true;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.map.setlobby");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("create")) {
						if(p.hasPermission("sabotage.map.create")) {
							p.sendMessage(ChatColor.RED + "Missing Args! " + ChatColor.DARK_RED + "/sab create <mapname>");
							return false;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.map.create");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("remove")) {
						if(p.hasPermission("sabotage.map.remove")) {
							p.sendMessage(ChatColor.RED + "Missing Args! " + ChatColor.DARK_RED + "/sab remove <mapname>");
							return false;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.map.remove");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("setspawn")) {
						if(p.hasPermission("sabotage.map.setspawn")) {
							p.sendMessage(ChatColor.RED + "Missing Args! " + ChatColor.DARK_RED + "/sab setspawn <mapname>");
							return false;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.map.setspawn");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("setcrates")) {
						if(p.hasPermission("sabotage.map.setcrates")) {
							p.sendMessage(ChatColor.RED + "Missing Args! " + ChatColor.DARK_RED + "/sab setspawn <mapname>");
							return false;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.map.setcrates");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("settester")) {
						if(p.hasPermission("sabotage.map.settester")) {
							p.sendMessage(ChatColor.RED + "Missing Args! " + ChatColor.DARK_RED + "/sab settester <mapname>");
							return false;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.map.settester");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("enable")) {
						if(p.hasPermission("sabotage.map.enable")) {
							p.sendMessage(ChatColor.RED + "Missing Args! " + ChatColor.DARK_RED + "/sab enable <mapname>");
							return false;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.map.enable");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("disable")) {
						if(p.hasPermission("sabotage.map.disable")) {
							p.sendMessage(ChatColor.RED + "Missing Args! " + ChatColor.DARK_RED + "/sab disable <mapname>");
							return false;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.map.disable");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("dev")) {
						if(p.hasPermission("sabotage.dev")) {
							if(!devs.contains(p)) {
								p.sendMessage(ChatColor.GREEN + "Entering Developer Mode...");
								p.sendMessage(ChatColor.GRAY + "Developer Mode removes some requirements for gameplay.");
								p.sendMessage(ChatColor.GRAY + "Glitches may appear in this mode. " + ChatColor.RED + "Use at your own risk!");
								p.playSound(p.getLocation(), Sound.BLOCK_IRON_DOOR_OPEN, 2F, 1F);
								devs.add(p);
								Schedule.forceStart = true;
								return true;
							} else {
								p.sendMessage(ChatColor.RED + "Exiting Developer Mode...");
								p.playSound(p.getLocation(), Sound.BLOCK_IRON_DOOR_CLOSE, 2F, 1F);
								devs.remove(p);
								Schedule.forceStart = false;
								return true;
							}
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.dev");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("karma")) {
						if(p.hasPermission("sabotage.karma")) {
							p.sendMessage(ChatColor.RED + "Missing Args! " + ChatColor.DARK_RED + "/sab karma <player> (add/remove) (amt)");
							return false;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.karma");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("pass")) {
						if(p.hasPermission("sabotage.pass")) {
							p.sendMessage(ChatColor.RED + "Missing Args! " + ChatColor.DARK_RED + "/sab pass <player> (add/remove) (amt)");
							return false;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.pass");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("start")) {
						if(p.hasPermission("sabotage.start")) {
							p.sendMessage(ChatColor.GREEN + "Now ignoring player requirements! ");
							p.playSound(p.getLocation(), Sound.BLOCK_STONE_BUTTON_CLICK_ON, 2F, 1F);
							Schedule.forceStart = true;
							return true;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.start");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("reload")) {
						if(p.hasPermission("sabotage.reload")) {
							p.sendMessage(ChatColor.DARK_GREEN + "Reloading Sabotage...");
							getLogger().info("Sabotage is performing a self-reload. A full server reload is required when updating this plugin.");
							SABTools.onDisable();
							this.reloadConfig();
							SABTools.onEnable();
							p.sendMessage(ChatColor.DARK_GREEN + "Reload Complete!");
							return true;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.reload");
							return false;
						}
					}
				}
				if(args.length == 2) {
					if(args[0].equalsIgnoreCase("help")) {
						if(p.hasPermission("sabotage.help")) {
							if(args[1].equalsIgnoreCase("1")) {
								p.sendMessage(ChatColor.GOLD + "\nSabotage by kNoAPP " + ChatColor.YELLOW + "Command List (1/3)");
								p.sendMessage(ChatColor.DARK_GREEN + "-----------------------------------------------");
								p.sendMessage(ChatColor.GRAY + "  /sab join " + ChatColor.WHITE + "- Join the Game");
								p.sendMessage(ChatColor.GRAY + "  /sab leave " + ChatColor.WHITE + "- Leave the Game");
								p.sendMessage(ChatColor.GRAY + "  /sab shop " + ChatColor.WHITE + "- In-Game Shop");
								p.sendMessage(ChatColor.GRAY + "  /sab setLobby " + ChatColor.WHITE + "- Set Sabotage Lobby");
								p.sendMessage(ChatColor.GRAY + "  /sab create <mapname> " + ChatColor.WHITE + "- Create a Map");
								p.sendMessage(ChatColor.GRAY + "  /sab remove <mapname> " + ChatColor.WHITE + "- Remove a Map");
								p.sendMessage(ChatColor.DARK_GREEN + "-----------------------------------------------");
								return true;
							}
							if(args[1].equalsIgnoreCase("2")) {
								p.sendMessage(ChatColor.GOLD + "\nSabotage by kNoAPP " + ChatColor.YELLOW + "Command List (2/3)");
								p.sendMessage(ChatColor.DARK_GREEN + "-----------------------------------------------");
								p.sendMessage(ChatColor.GRAY + "  /sab setSpawn <mapname> " + ChatColor.WHITE + "- Set Map Spawn");
								p.sendMessage(ChatColor.GRAY + "  /sab setCrates <mapname> " + ChatColor.WHITE + "- Get Chest Set Tool");
								p.sendMessage(ChatColor.GRAY + "  /sab setTester <mapname> " + ChatColor.WHITE + "- Set Tester");
								p.sendMessage(ChatColor.GRAY + "  /sab enable <mapname> " + ChatColor.WHITE + "- Enable a Map");
								p.sendMessage(ChatColor.GRAY + "  /sab disable <mapname> " + ChatColor.WHITE + "- Disable a Map");
								p.sendMessage(ChatColor.GRAY + "  /sab dev " + ChatColor.WHITE + "- Developer Mode (Unstable)");
								p.sendMessage(ChatColor.DARK_GREEN + "-----------------------------------------------");
								return true;
							}
							if(args[1].equalsIgnoreCase("3")) {
								p.sendMessage(ChatColor.GOLD + "\nSabotage by kNoAPP " + ChatColor.YELLOW + "Command List (3/3)");
								p.sendMessage(ChatColor.DARK_GREEN + "-----------------------------------------------");
								p.sendMessage(ChatColor.GRAY + "  /sab karma <player> (add/remove) (amt) " + ChatColor.WHITE + "- Karma");
								p.sendMessage(ChatColor.GRAY + "  /sab pass <player> (add/remove) (amt) " + ChatColor.WHITE + "- Saboteur Passes");
								p.sendMessage(ChatColor.GRAY + "  /sab start " + ChatColor.WHITE + "- Force Start the Game");
								p.sendMessage(ChatColor.GRAY + "  /sab reload " + ChatColor.WHITE + "- Reload the Plugin");
								p.sendMessage(ChatColor.GRAY + " ");
								p.sendMessage(ChatColor.GRAY + " ");
								p.sendMessage(ChatColor.DARK_GREEN + "-----------------------------------------------");
								return true;
							}
							p.sendMessage(ChatColor.RED + "Invalid Help Page!");
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.help");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("create")) {
						if(p.hasPermission("sabotage.map.create")) {
							if(Map.validMap(args[1])) {
								p.sendMessage(ChatColor.RED + "The map " + ChatColor.DARK_RED + args[1] + ChatColor.RED + " already exists!");
								return false;
							} else {
								if(Map.getNextOpenID() == 0) {
									p.sendMessage(ChatColor.RED + "Map limit reached! Delete some then try again!");
									p.playSound(p.getLocation(), Sound.ENTITY_CHICKEN_HURT, 2F, 1F);
									return false;
								}
								Map.createMap(args[1]);
								p.sendMessage(ChatColor.GREEN + "Map " + ChatColor.DARK_GREEN + args[1] + ChatColor.GREEN + " was successfully created!");
								p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 2F, 1F);
								return true;
							}
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.map.create");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("remove")) {
						if(p.hasPermission("sabotage.map.remove")) {
							if(Map.validMap(args[1])) {
								Map.removeMap(args[1]);
								p.sendMessage(ChatColor.GREEN + "Map " + ChatColor.DARK_GREEN + args[1] + ChatColor.GREEN + " was successfully removed!");
								p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 2F, 1F);
								return true;
							} else {
								p.sendMessage(ChatColor.RED + "The map " + ChatColor.DARK_RED + args[1] + ChatColor.RED + " isn't a vaild map!");
								return false;
							}
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.map.remove");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("buy")) {
						if(p.hasPermission("sabotage.shop")) {
							if(playersPlaying.contains(p) && gamePhase == 2) {
								try {
									int selection = Integer.parseInt(args[1]);
									ShopItem si = ShopItem.getItemByID(selection, Role.playersRole.get(p));
									if(si != null) {
										if(si.canActivate(p)) {
											if(p.getGameMode() != GameMode.SPECTATOR || si == ShopItem.getItemByID(5, Role.INNOCENT)) {
												si.activate(p);
												return true;
											} else {
												p.sendMessage(ChatColor.RED + "You may not buy this right now!");
												return false;
											}
										}
										p.sendMessage(ChatColor.DARK_RED + "You do not have enough Karma!");
										return false;
									}
									return true;
								} catch (Exception ex) {
									p.sendMessage(ChatColor.RED + "Invalid Number! " + ChatColor.DARK_RED + "/sab buy <ID>");
									return false;
								}
							}
							p.sendMessage(ChatColor.RED + "You cannot use this right now!");
							return false;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.shop");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("setSpawn")) {
						if(p.hasPermission("sabotage.map.setspawn")) {
							if(Map.validMap(args[1])) {
								Map.getMapByName(args[1]).setSpawnLocation(p.getLocation());
								p.sendMessage(ChatColor.GREEN + "Spawnpoint for " + ChatColor.DARK_GREEN + args[1] + ChatColor.GREEN + " stored as your location!");
								return true;
							} else {
								p.sendMessage(ChatColor.RED + "Map " + ChatColor.DARK_RED + args[1] + ChatColor.RED + " is not a valid map!");
								return false;
							}
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.map.setspawn");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("setcrates")) {
						if(p.hasPermission("sabotage.map.setcrates")) {
							if(Map.validMap(args[1])) {
								p.getInventory().clear();
								p.getInventory().addItem(ItemManagement.getCrateItem(Map.getMapByName(args[1]), 1));
								p.getInventory().addItem(ItemManagement.getCrateItem(Map.getMapByName(args[1]), 2));
								p.sendMessage(ChatColor.GREEN + "Here you go!");
								return true;
							} else {
								p.sendMessage(ChatColor.RED + "Map " + ChatColor.DARK_RED + args[1] + ChatColor.RED + " is not a valid map!");
								return false;
							}
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.map.setcrates");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("settester")) {
						if(p.hasPermission("sabotage.map.settester")) {
							if(Map.validMap(args[1])) {
								p.getInventory().clear();
								p.getInventory().addItem(ItemManagement.getSignItem(Map.getMapByName(args[1])));
								p.getInventory().addItem(ItemManagement.getBarsItem(Map.getMapByName(args[1])));
								p.getInventory().addItem(ItemManagement.getWoolItem(Map.getMapByName(args[1])));
								p.sendMessage(ChatColor.GREEN + "Here you go!");
								return true;
							} else {
								p.sendMessage(ChatColor.RED + "Map " + ChatColor.DARK_RED + args[1] + ChatColor.RED + " is not a valid map!");
								return false;
							}
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.map.settester");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("enable")) {
						if(p.hasPermission("sabotage.map.enable")) {
							if(Map.validMap(args[1])) {
								Map.getMapByName(args[1]).setStatus(true);
								p.sendMessage(ChatColor.GREEN + "Map " + ChatColor.DARK_GREEN + args[1] + ChatColor.GREEN + " has been enabled!");
								return true;
							}
							p.sendMessage(ChatColor.RED + "Invalid map!");
							return false;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.map.enable");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("disable")) {
						if(p.hasPermission("sabotage.map.disable")) {
							if(Map.validMap(args[1])) {
								Map.getMapByName(args[1]).setStatus(false);
								p.sendMessage(ChatColor.GREEN + "Map " + ChatColor.DARK_GREEN + args[1] + ChatColor.GREEN + " has been disabled!");
								return true;
							}
							p.sendMessage(ChatColor.RED + "Invalid map!");
							return false;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.map.disable");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("karma")) {
						if(p.hasPermission("sabotage.karma")) {
							p.sendMessage(ChatColor.GREEN + "Karma: " + ChatColor.GRAY + Karma.getKarma(args[1]));
							return true;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.karma");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("pass")) {
						if(p.hasPermission("sabotage.pass")) {
							p.sendMessage(ChatColor.GREEN + "Passes: " + ChatColor.GRAY + Passes.getSabPasses(args[1]));
							return true;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.pass");
							return false;
						}
					}
				}
				if(args.length == 3) {
					if(args[0].equalsIgnoreCase("karma")) {
						if(p.hasPermission("sabotage.karma")) {
							p.sendMessage(ChatColor.RED + "Missing Args! " + ChatColor.DARK_RED + "/sab karma <player> (add/remove) (amt)");
							return false;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.karma");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("pass")) {
						if(p.hasPermission("sabotage.pass")) {
							p.sendMessage(ChatColor.RED + "Missing Args! " + ChatColor.DARK_RED + "/sab pass <player> (add/remove) (amt)");
							return false;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.pass");
							return false;
						}
					}
				}
				if(args.length == 4) {
					if(args[0].equalsIgnoreCase("karma")) {
						if(p.hasPermission("sabotage.karma")) {
							int amtAsked = 0;
							try {
								amtAsked = Integer.parseInt(args[3]);
							} catch (Exception ex) {
								p.sendMessage(ChatColor.RED + "Entry " + ChatColor.DARK_RED + args[3] + ChatColor.RED + " is not a valid number!");
								return false;
							}
							
							if(args[2].equalsIgnoreCase("add")) {
								Karma.addKarma(args[1], amtAsked);
								p.sendMessage(ChatColor.GREEN + "Updated Karma: " + ChatColor.GRAY + Karma.getKarma(args[1]));
								if(Bukkit.getPlayerExact(args[1]) != null) {
									Bukkit.getPlayerExact(args[1]).sendMessage(ChatColor.DARK_GREEN + p.getName() + ChatColor.GREEN + " added " + ChatColor.YELLOW + amtAsked + ChatColor.GREEN + " karma to your account!");
									Bukkit.getPlayerExact(args[1]).playSound(Bukkit.getPlayerExact(args[1]).getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 2F, 1F);
								}
								return true;
							}
							if(args[2].equalsIgnoreCase("remove")) {
								Karma.removeKarma(args[1], amtAsked);
								p.sendMessage(ChatColor.GREEN + "Updated Karma: " + ChatColor.GRAY + Karma.getKarma(args[1]));
								if(Bukkit.getPlayerExact(args[1]) != null) {
									Bukkit.getPlayerExact(args[1]).sendMessage(ChatColor.DARK_RED + p.getName() + ChatColor.RED + " removed " + ChatColor.YELLOW + amtAsked + ChatColor.RED + " karma from your account!");
									Bukkit.getPlayerExact(args[1]).playSound(Bukkit.getPlayerExact(args[1]).getLocation(), Sound.ENTITY_SPIDER_DEATH, 2F, 1F);
									if(playersPlaying.contains(Bukkit.getPlayerExact(args[1])) && Karma.getKarma(Bukkit.getPlayerExact(args[1]).getName()) <= 0) {
										Karma.addKarma(Bukkit.getPlayerExact(args[1]).getName(), -1 * Karma.getKarma(Bukkit.getPlayerExact(args[1]).getName()));
										SABTools.onLeave(Bukkit.getPlayerExact(args[1]), false);
										Bukkit.getPlayerExact(args[1]).sendMessage(ChatColor.DARK_RED + "You have run out of Karma! Come back tomorrow for more!");
										Bukkit.getPlayerExact(args[1]).playSound(Bukkit.getPlayerExact(args[1]).getLocation(), Sound.ENTITY_WITHER_SHOOT, 2F, 0.5F);
									}
								}
								return true;
							}
							p.sendMessage(ChatColor.RED + "Bad Args! " + ChatColor.DARK_RED + "/sab karma <player> (add/remove) (amt)");
							return false;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.karma");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("pass")) {
						if(p.hasPermission("sabotage.pass")) {
							int amtAsked = 0;
							try {
								amtAsked = Integer.parseInt(args[3]);
							} catch (Exception ex) {
								p.sendMessage(ChatColor.RED + "Entry " + ChatColor.DARK_RED + args[3] + ChatColor.RED + " is not a valid number!");
								return false;
							}
							
							if(args[2].equalsIgnoreCase("add")) {
								Passes.addSabPasses(args[1], amtAsked);
								p.sendMessage(ChatColor.GREEN + "Updated Passes: " + ChatColor.GRAY + Passes.getSabPasses(args[1]));
								if(Bukkit.getPlayerExact(args[1]) != null) {
									Bukkit.getPlayerExact(args[1]).sendMessage(ChatColor.DARK_GREEN + p.getName() + ChatColor.GREEN + " added " + ChatColor.YELLOW + amtAsked + ChatColor.GREEN + " Saboteur Passes to your account!");
									Bukkit.getPlayerExact(args[1]).playSound(Bukkit.getPlayerExact(args[1]).getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 2F, 1F);
									if(playersPlaying.contains(Bukkit.getPlayerExact(args[1])) && gamePhase == 0) {
										SABTools.onLeave(Bukkit.getPlayerExact(args[1]), false);
										this.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
								    		public void run() {
								    			SABTools.onJoin(Bukkit.getPlayerExact(args[1]));
								    		}
										}, 20L);
									}
								}
								return true;
							}
							if(args[2].equalsIgnoreCase("remove")) {
								Passes.removeSabPasses(args[1], amtAsked);
								p.sendMessage(ChatColor.GREEN + "Updated Passes: " + ChatColor.GRAY + Passes.getSabPasses(args[1]));
								if(Bukkit.getPlayerExact(args[1]) != null) {
									Bukkit.getPlayerExact(args[1]).sendMessage(ChatColor.DARK_RED + p.getName() + ChatColor.RED + " removed " + ChatColor.YELLOW + amtAsked + ChatColor.RED + " Saboteur Passes from your account!");
									Bukkit.getPlayerExact(args[1]).playSound(Bukkit.getPlayerExact(args[1]).getLocation(), Sound.ENTITY_SPIDER_DEATH, 2F, 1F);
									if(playersPlaying.contains(Bukkit.getPlayerExact(args[1])) && gamePhase == 0) {
										SABTools.onLeave(Bukkit.getPlayerExact(args[1]), false);
										this.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
								    		public void run() {
								    			SABTools.onJoin(Bukkit.getPlayerExact(args[1]));
								    		}
										}, 20L);
									}
								}
								return true;
							}
							p.sendMessage(ChatColor.RED + "Bad Args! " + ChatColor.DARK_RED + "/sab pass <player> (add/remove) (amt)");
							return false;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.pass");
							return false;
						}
					}
				}
				p.sendMessage(ChatColor.RED + "Unknown Sabotage Command! " + ChatColor.DARK_RED + "/sab help");
				return false;
			}
		}
		if(sender instanceof ConsoleCommandSender) {
			ConsoleCommandSender p = (ConsoleCommandSender) sender;
			if(cmd.getName().equalsIgnoreCase("sab")) {
				if(args.length == 0) {
					p.sendMessage(ChatColor.GREEN + "Sabotage " + ChatColor.WHITE + "- By kNoAPP");
					p.sendMessage(ChatColor.GRAY + "/sab help for Sabotage Commands");
					return true;
				}
				if(args.length == 1) {
					if(args[0].equalsIgnoreCase("help")) {
						if(p.hasPermission("sabotage.help")) {
							p.sendMessage(ChatColor.GOLD + "\nSabotage by kNoAPP " + ChatColor.YELLOW + "Console Command List (1/1)");
							p.sendMessage(ChatColor.DARK_GREEN + "-----------------------------------------------");
							p.sendMessage(ChatColor.GRAY + "  /sab create <mapname> " + ChatColor.WHITE + "- Create a Map");
							p.sendMessage(ChatColor.GRAY + "  /sab remove <mapname> " + ChatColor.WHITE + "- Remove a Map");
							p.sendMessage(ChatColor.GRAY + "  /sab enable <mapname> " + ChatColor.WHITE + "- Enable a Map");
							p.sendMessage(ChatColor.GRAY + "  /sab disable <mapname> " + ChatColor.WHITE + "- Disable a Map");
							p.sendMessage(ChatColor.GRAY + "  /sab karma <player> (add/remove) (amt) " + ChatColor.WHITE + "- Karma");
							p.sendMessage(ChatColor.GRAY + "  /sab pass <player> (add/remove) (amt) " + ChatColor.WHITE + "- Saboteur Passes");
							p.sendMessage(ChatColor.GRAY + "  /sab start " + ChatColor.WHITE + "- Force Start the Game");
							p.sendMessage(ChatColor.GRAY + "  /sab reload " + ChatColor.WHITE + "- Reload the Plugin");
							p.sendMessage(ChatColor.DARK_GREEN + "-----------------------------------------------");
							return true;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.help");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("create")) {
						if(p.hasPermission("sabotage.map.create")) {
							p.sendMessage(ChatColor.RED + "Missing Args! " + ChatColor.DARK_RED + "/sab create <mapname>");
							return false;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.map.create");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("remove")) {
						if(p.hasPermission("sabotage.map.remove")) {
							p.sendMessage(ChatColor.RED + "Missing Args! " + ChatColor.DARK_RED + "/sab remove <mapname>");
							return false;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.map.remove");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("enable")) {
						if(p.hasPermission("sabotage.map.enable")) {
							p.sendMessage(ChatColor.RED + "Missing Args! " + ChatColor.DARK_RED + "/sab enable <mapname>");
							return false;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.map.enable");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("disable")) {
						if(p.hasPermission("sabotage.map.disable")) {
							p.sendMessage(ChatColor.RED + "Missing Args! " + ChatColor.DARK_RED + "/sab disable <mapname>");
							return false;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.map.disable");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("karma")) {
						if(p.hasPermission("sabotage.karma")) {
							p.sendMessage(ChatColor.RED + "Missing Args! " + ChatColor.DARK_RED + "/sab karma <player> (add/remove) (amt)");
							return false;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.karma");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("pass")) {
						if(p.hasPermission("sabotage.pass")) {
							p.sendMessage(ChatColor.RED + "Missing Args! " + ChatColor.DARK_RED + "/sab pass <player> (add/remove) (amt)");
							return false;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.pass");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("start")) {
						if(p.hasPermission("sabotage.start")) {
							p.sendMessage(ChatColor.GREEN + "Now ignoring player requirements! ");
							Schedule.forceStart = true;
							return true;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.start");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("reload")) {
						if(p.hasPermission("sabotage.reload")) {
							p.sendMessage(ChatColor.GREEN + "Requested Reload...");
							SABTools.onDisable();
							this.reloadConfig();
							SABTools.onEnable();
							p.sendMessage(ChatColor.DARK_GREEN + "Reload Complete!");
							return true;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.reload");
							return false;
						}
					}
				}
				if(args.length == 2) {
					if(args[0].equalsIgnoreCase("create")) {
						if(p.hasPermission("sabotage.map.create")) {
							if(Map.validMap(args[1])) {
								p.sendMessage(ChatColor.RED + "The map " + ChatColor.DARK_RED + args[1] + ChatColor.RED + " already exists!");
								return false;
							} else {
								if(Map.getNextOpenID() == 0) {
									p.sendMessage(ChatColor.RED + "Map limit reached! Delete some then try again!");
									return false;
								}
								Map.createMap(args[1]);
								p.sendMessage(ChatColor.GREEN + "Map " + ChatColor.DARK_GREEN + args[1] + ChatColor.GREEN + " was successfully created!");
								return true;
							}
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.map.create");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("remove")) {
						if(p.hasPermission("sabotage.map.remove")) {
							if(Map.validMap(args[1])) {
								Map.removeMap(args[1]);
								p.sendMessage(ChatColor.GREEN + "Map " + ChatColor.DARK_GREEN + args[1] + ChatColor.GREEN + " was successfully removed!");
								return true;
							} else {
								p.sendMessage(ChatColor.RED + "The map " + ChatColor.DARK_RED + args[1] + ChatColor.RED + " isn't a vaild map!");
								return false;
							}
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.map.remove");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("enable")) {
						if(p.hasPermission("sabotage.map.enable")) {
							if(Map.validMap(args[1])) {
								Map.getMapByName(args[1]).setStatus(true);
								p.sendMessage(ChatColor.GREEN + "Map " + ChatColor.DARK_GREEN + args[1] + ChatColor.GREEN + " has been enabled!");
								return true;
							}
							p.sendMessage(ChatColor.RED + "Invalid map!");
							return false;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.map.enable");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("disable")) {
						if(p.hasPermission("sabotage.map.disable")) {
							if(Map.validMap(args[1])) {
								Map.getMapByName(args[1]).setStatus(false);
								p.sendMessage(ChatColor.GREEN + "Map " + ChatColor.DARK_GREEN + args[1] + ChatColor.GREEN + " has been disabled!");
								return true;
							}
							p.sendMessage(ChatColor.RED + "Invalid map!");
							return false;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.map.disable");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("karma")) {
						if(p.hasPermission("sabotage.karma")) {
							p.sendMessage(ChatColor.GREEN + "Karma: " + ChatColor.GRAY + Karma.getKarma(args[1]));
							return true;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.karma");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("pass")) {
						if(p.hasPermission("sabotage.pass")) {
							p.sendMessage(ChatColor.GREEN + "Passes: " + ChatColor.GRAY + Passes.getSabPasses(args[1]));
							return true;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.pass");
							return false;
						}
					}
				}
				if(args.length == 3) {
					if(args[0].equalsIgnoreCase("karma")) {
						if(p.hasPermission("sabotage.karma")) {
							p.sendMessage(ChatColor.RED + "Missing Args! " + ChatColor.DARK_RED + "/sab karma <player> (add/remove) (amt)");
							return false;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.karma");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("pass")) {
						if(p.hasPermission("sabotage.pass")) {
							p.sendMessage(ChatColor.RED + "Missing Args! " + ChatColor.DARK_RED + "/sab pass <player> (add/remove) (amt)");
							return false;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.pass");
							return false;
						}
					}
				}
				if(args.length == 4) {
					if(args[0].equalsIgnoreCase("karma")) {
						if(p.hasPermission("sabotage.karma")) {
							int amtAsked = 0;
							try {
								amtAsked = Integer.parseInt(args[3]);
							} catch (Exception ex) {
								p.sendMessage(ChatColor.RED + "Entry " + ChatColor.DARK_RED + args[3] + ChatColor.RED + " is not a valid number!");
								return false;
							}
							
							if(args[2].equalsIgnoreCase("add")) {
								Karma.addKarma(args[1], amtAsked);
								p.sendMessage(ChatColor.GREEN + "Updated Karma: " + ChatColor.GRAY + Karma.getKarma(args[1]));
								if(Bukkit.getPlayerExact(args[1]) != null) {
									Bukkit.getPlayerExact(args[1]).sendMessage(ChatColor.DARK_GREEN + p.getName() + ChatColor.GREEN + " added " + ChatColor.YELLOW + amtAsked + ChatColor.GREEN + " karma to your account!");
									Bukkit.getPlayerExact(args[1]).playSound(Bukkit.getPlayerExact(args[1]).getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 2F, 1F);
								}
								return true;
							}
							if(args[2].equalsIgnoreCase("remove")) {
								Karma.removeKarma(args[1], amtAsked);
								p.sendMessage(ChatColor.GREEN + "Updated Karma: " + ChatColor.GRAY + Karma.getKarma(args[1]));
								if(Bukkit.getPlayerExact(args[1]) != null) {
									Bukkit.getPlayerExact(args[1]).sendMessage(ChatColor.DARK_RED + p.getName() + ChatColor.RED + " removed " + ChatColor.YELLOW + amtAsked + ChatColor.RED + " karma from your account!");
									Bukkit.getPlayerExact(args[1]).playSound(Bukkit.getPlayerExact(args[1]).getLocation(), Sound.ENTITY_SPIDER_DEATH, 2F, 1F);
									if(playersPlaying.contains(Bukkit.getPlayerExact(args[1])) && Karma.getKarma(Bukkit.getPlayerExact(args[1]).getName()) <= 0) {
										Karma.addKarma(Bukkit.getPlayerExact(args[1]).getName(), -1 * Karma.getKarma(Bukkit.getPlayerExact(args[1]).getName()));
										SABTools.onLeave(Bukkit.getPlayerExact(args[1]), false);
										Bukkit.getPlayerExact(args[1]).sendMessage(ChatColor.DARK_RED + "You have run out of Karma! Come back tomorrow for more!");
										Bukkit.getPlayerExact(args[1]).playSound(Bukkit.getPlayerExact(args[1]).getLocation(), Sound.ENTITY_WITHER_SHOOT, 2F, 0.5F);
									}
								}
								return true;
							}
							p.sendMessage(ChatColor.RED + "Bad Args! " + ChatColor.DARK_RED + "/sab karma <player> (add/remove) (amt)");
							return false;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.karma");
							return false;
						}
					}
					if(args[0].equalsIgnoreCase("pass")) {
						if(p.hasPermission("sabotage.pass")) {
							int amtAsked = 0;
							try {
								amtAsked = Integer.parseInt(args[3]);
							} catch (Exception ex) {
								p.sendMessage(ChatColor.RED + "Entry " + ChatColor.DARK_RED + args[3] + ChatColor.RED + " is not a valid number!");
								return false;
							}
							
							if(args[2].equalsIgnoreCase("add")) {
								Passes.addSabPasses(args[1], amtAsked);
								p.sendMessage(ChatColor.GREEN + "Updated Passes: " + ChatColor.GRAY + Passes.getSabPasses(args[1]));
								if(Bukkit.getPlayerExact(args[1]) != null) {
									Bukkit.getPlayerExact(args[1]).sendMessage(ChatColor.DARK_GREEN + p.getName() + ChatColor.GREEN + " added " + ChatColor.YELLOW + amtAsked + ChatColor.GREEN + " Saboteur Passes to your account!");
									Bukkit.getPlayerExact(args[1]).playSound(Bukkit.getPlayerExact(args[1]).getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 2F, 1F);
									if(playersPlaying.contains(Bukkit.getPlayerExact(args[1])) && gamePhase == 0) {
										SABTools.onLeave(Bukkit.getPlayerExact(args[1]), false);
										this.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
								    		public void run() {
								    			SABTools.onJoin(Bukkit.getPlayerExact(args[1]));
								    		}
										}, 20L);
									}
								}
								return true;
							}
							if(args[2].equalsIgnoreCase("remove")) {
								Passes.removeSabPasses(args[1], amtAsked);
								p.sendMessage(ChatColor.GREEN + "Updated Passes: " + ChatColor.GRAY + Passes.getSabPasses(args[1]));
								if(Bukkit.getPlayerExact(args[1]) != null) {
									Bukkit.getPlayerExact(args[1]).sendMessage(ChatColor.DARK_RED + p.getName() + ChatColor.RED + " removed " + ChatColor.YELLOW + amtAsked + ChatColor.RED + " Saboteur Passes from your account!");
									Bukkit.getPlayerExact(args[1]).playSound(Bukkit.getPlayerExact(args[1]).getLocation(), Sound.ENTITY_SPIDER_DEATH, 2F, 1F);
									if(playersPlaying.contains(Bukkit.getPlayerExact(args[1])) && gamePhase == 0) {
										SABTools.onLeave(Bukkit.getPlayerExact(args[1]), false);
										this.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
								    		public void run() {
								    			SABTools.onJoin(Bukkit.getPlayerExact(args[1]));
								    		}
										}, 20L);
									}
								}
								return true;
							}
							p.sendMessage(ChatColor.RED + "Bad Args! " + ChatColor.DARK_RED + "/sab pass <player> (add/remove) (amt)");
							return false;
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.pass");
							return false;
						}
					}
				}
				p.sendMessage(ChatColor.RED + "Unknown Sabotage Command! " + ChatColor.DARK_RED + "/sab help");
				return false;
			}
		}
		return false;
	}
	
	@EventHandler
	public void onCommandPreProcess(PlayerCommandPreprocessEvent e) {
		Player p = e.getPlayer();
		String s = e.getMessage().toLowerCase();
		if(s.startsWith("/tell") || s.startsWith("/msg") || s.startsWith("/w") || s.startsWith("/whisper")) {
			if(!p.hasPermission("sabotage.whisper")) {
				if(playersPlaying.contains(p) && p.getGameMode() == GameMode.SPECTATOR && gamePhase != 0) {
					p.sendMessage(ChatColor.RED + "You may not private message right now!");
					e.setCancelled(true);
				}
			}
		}
		if(Bukkit.getPluginManager().getPlugin("kNoCore") != null) {
			if(s.startsWith("/knc menu")) {
				if(playersPlaying.contains(p) && p.getGameMode() == GameMode.SPECTATOR && gamePhase != 0) {
					p.sendMessage(ChatColor.RED + "Your gadgets are disabled for now.");
					e.setCancelled(true);
				}
			}
		}
	}
	
	@EventHandler
	public void onPlayerLeave(PlayerQuitEvent e) {
		Player p = e.getPlayer();
		SABTools.onLeave(p, true);
		if(devs.contains(p)) {
			devs.remove(p);
		}
	}
	
	@EventHandler
	public void onPlayerClick(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if(!playersPlaying.contains(p)) {
			return;
		}
		if(e.getHand() == EquipmentSlot.HAND && (e.getAction() == Action.RIGHT_CLICK_BLOCK || e.getAction() == Action.RIGHT_CLICK_AIR)) {
			if(playersPlaying.contains(p) && p.getGameMode() == GameMode.SPECTATOR) {
				e.setCancelled(true);
				return;
			}
			if(e.getAction() == Action.RIGHT_CLICK_BLOCK) {
				Block b = e.getClickedBlock();
				if(gamePhase != 0) {
					for(Crate c : Crate.crates) {
						if(c.getMap().getName() == Map.currentlySelectedMap.getName()) {
							if(SABTools.compareLocationXYZ(c.getX(), c.getY(), c.getZ(), b.getLocation()) == true) {
								b.setType(Material.AIR);
								Loot l = Loot.selectLoot(c.getLevel());
								if(l == Loot.BOW || l == Loot.BOW2) {
									p.getInventory().addItem(l.generateItem());
									p.getInventory().addItem(new ItemStack(Material.ARROW, 32));
								} else if(l == Loot.TNT){
									p.getInventory().addItem(l.generateItem());
									p.getInventory().addItem(l.generateItem());
									p.getInventory().addItem(l.generateItem());
									p.getInventory().addItem(l.generateItem());
									p.getInventory().addItem(l.generateItem());
								} else {
									p.getInventory().addItem(l.generateItem());
								}
							}
						}
					}
					if(gamePhase != 1) {
						for(TesterSign ts : TesterSign.TSs) {
							if(ts.getMap().getName().equalsIgnoreCase(Map.currentlySelectedMap.getName())) {
								if(SABTools.compareLocationXYZ(ts.getX(), ts.getY(), ts.getZ(), b.getLocation()) == true) {
									if(TesterSign.inUse == false) {
										TesterSign.activate(p, 15, 0L);
									}
								}
							}
						}
					}
				}
			}
			if(p.getInventory().getItemInMainHand() != null) {
				if(p.getInventory().getItemInMainHand().isSimilar(ItemManagement.getMapSelector())) {
					if(p.hasPermission("sabotage.map.vote")) {
						MapVoting.openVotingInv(p);
					} else {
						p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.map.vote");
					}
					return;
				}
				if(p.getInventory().getItemInMainHand().isSimilar(ItemManagement.getNoSabPass())) {
					p.sendMessage(ChatColor.RED + "You don't have any Saboteur Passes!");
					p.playSound(p.getLocation(), Sound.ENTITY_CHICKEN_HURT, 2F, 1F);
					return;
				}
				if(p.getInventory().getItemInMainHand().isSimilar(ItemManagement.getSabPass(false, Data.PLAYER.getFileConfig().getInt("Player." + p.getUniqueId() + ".SaboteurPasses")))) {
					p.playSound(p.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 2F, 1F);
					p.sendMessage(ChatColor.GRAY + "Saboteur Pass: " + ChatColor.GREEN + "" + ChatColor.BOLD + "Active");
					p.getInventory().setItem(8, ItemManagement.getSabPass(true, Data.PLAYER.getFileConfig().getInt("Player." + p.getUniqueId() + ".SaboteurPasses")));
					playersWithPasses.add(p);
					return;
				}
				if(p.getInventory().getItemInMainHand().isSimilar(ItemManagement.getSabPass(true, Data.PLAYER.getFileConfig().getInt("Player." + p.getUniqueId() + ".SaboteurPasses")))) {
					p.playSound(p.getLocation(), Sound.ENTITY_SPIDER_DEATH, 2F, 1F);
					p.sendMessage(ChatColor.GRAY + "Saboteur Pass: " + ChatColor.RED + "" + ChatColor.BOLD + "Inactive");
					p.getInventory().setItem(8, ItemManagement.getSabPass(false, Data.PLAYER.getFileConfig().getInt("Player." + p.getUniqueId() + ".SaboteurPasses")));
					playersWithPasses.remove(p);
					return;
				}
			}
		}
	}
	
	@EventHandler
	public void onChestOpen(InventoryOpenEvent e) {
		Player p = (Player) e.getPlayer();
		if(e.getInventory().getHolder() instanceof Chest || e.getInventory().getHolder() instanceof DoubleChest) {
			if(playersPlaying.contains(p) && (gamePhase == 1 || gamePhase == 2 || gamePhase == 3)) {
				e.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onInvClick(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked(); // The player that clicked the item
		ItemStack clicked = e.getCurrentItem(); // The item that was clicked
		//Inventory inv = e.getInventory(); // The inventory that was clicked in
		
		if(clicked == null) {
			return;
		}
		
		if(clicked.isSimilar(ItemManagement.getMapSelector()) || clicked.isSimilar(ItemManagement.getKarmaCount(p)) || clicked.isSimilar(ItemManagement.getSabPass(true, Data.PLAYER.getFileConfig().getInt("Player." + p.getUniqueId() + ".SaboteurPasses"))) || clicked.isSimilar(ItemManagement.getSabPass(false, Data.PLAYER.getFileConfig().getInt("Player." + p.getUniqueId() + ".SaboteurPasses"))) || clicked.isSimilar(ItemManagement.getNoSabPass())) {
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onTntExplosion(EntityExplodeEvent e) {
		if(e.getEntityType() == EntityType.PRIMED_TNT) {
			if(gamePhase != 0) {
				if(e.getLocation().distance(Map.currentlySelectedMap.getSpawnLocation()) < 200) {
					if(Data.MAIN.getFileConfig().getBoolean("World.EnableTNTLandDamage") == false) {
						e.blockList().clear();
						for(Player pl : playersPlaying) {
							if(pl.getGameMode() != GameMode.SPECTATOR && pl.getLocation().distance(e.getLocation()) < 5) {
								pl.damage(20 - (pl.getLocation().distance(e.getLocation()) * 4), e.getEntity());
							}
						}
					}
				}
			}
		}
	}
	
	@EventHandler
	public void onFireSpread(BlockSpreadEvent e) {
		if(gamePhase != 0) {
			if(e.getBlock().getWorld() == Map.currentlySelectedMap.getSpawnLocation().getWorld()) {
				if(e.getBlock().getLocation().distance(Map.currentlySelectedMap.getSpawnLocation()) < 200) {
					e.setCancelled(true);
				}
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerPlaceBlock(BlockPlaceEvent e) {
		Player p = e.getPlayer();
		Block b = e.getBlockPlaced();
		if(knc && !playersPlaying.contains(p)) {
			if(!Rank.activePlayerRanks(p.getName()).contains(Rank.ADMIN) && !Rank.activePlayerRanks(p.getName()).contains(Rank.OWNER)) {
				e.setCancelled(true);
			}
		}
		if(playersPlaying.contains(p)) {
			if(gamePhase == 0 || gamePhase == 1 || gamePhase == 2 || gamePhase == 3) {
				e.setCancelled(true);
			}
		}
		if(p.getInventory().getItemInMainHand() != null) {
			ItemStack is = p.getInventory().getItemInMainHand();
			if(is.isSimilar(ItemManagement.getSurpriseItem()) && gamePhase == 2) {
				for(Crate c : Crate.crates) {
					if(c.getMap().getName() == Map.currentlySelectedMap.getName()) {
						if(SABTools.compareLocationXYZ(c.getX(), c.getY(), c.getZ(), b.getLocation()) == true) {
							p.sendMessage(ChatColor.RED + "You cannot place a Surprise here!");
							e.setCancelled(true);
							return;
						}
					}
				}
				for(IronBar ib : IronBar.ironbars) {
					if(ib.getMap().getName() == Map.currentlySelectedMap.getName()) {
						if(SABTools.compareLocationXYZ(ib.getX(), ib.getY(), ib.getZ(), b.getLocation()) == true) {
							p.sendMessage(ChatColor.RED + "You cannot place a Surprise here!");
							e.setCancelled(true);
							return;
						}
					}
				}
				e.setCancelled(false);
				p.sendMessage(ChatColor.DARK_RED + "Armed!");
				Actions.trapChest.add(b);
				return;
			}
			if(p.getInventory().getItemInMainHand().isSimilar(Loot.TNT.generateItem()) && gamePhase != 0 && gamePhase != 3) {
				e.setCancelled(false);
				TNTPrimed tnt = p.getWorld().spawn(e.getBlock().getLocation(), TNTPrimed.class);
				tnt.setYield(3F);
				p.getWorld().getBlockAt(b.getLocation()).setType(Material.AIR);
			}
			if(p.getInventory().getItemInMainHand().getType() == Material.CHEST || p.getInventory().getItemInMainHand().getType() == Material.ENDER_CHEST) {
				for(Map map : Map.maps) {
					if(p.getInventory().getItemInMainHand().isSimilar(ItemManagement.getCrateItem(map, 1)) || p.getInventory().getItemInMainHand().isSimilar(ItemManagement.getCrateItem(map, 2))) {
						if(p.hasPermission("sabotage.map.setcrates")) {
							Crate.createCrate(map, b.getX(), b.getY(), b.getZ(), b.getType(), b.getData());
							p.sendMessage(ChatColor.GREEN + "Regestered Crate as " + ChatColor.DARK_GREEN + "ID " + Crate.getCrateByLocation(b.getLocation()).getID() + ChatColor.GREEN + " under map " + ChatColor.YELLOW + map.getName() + ".");
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.map.setcrates");
							p.getInventory().setItemInMainHand(new ItemStack(Material.AIR, 1));
						}
					}
				}
			}
			if(p.getInventory().getItemInMainHand().getType() == Material.SIGN || p.getInventory().getItemInMainHand().getType() == Material.SIGN_POST || p.getInventory().getItemInMainHand().getType() == Material.WALL_SIGN) {
				for(Map map : Map.maps) {
					if(p.getInventory().getItemInMainHand().isSimilar(ItemManagement.getSignItem(map))) {
						if(p.hasPermission("sabotage.map.settester")) {
							TesterSign.createTS(map, b.getX(), b.getY(), b.getZ());
							p.sendMessage(ChatColor.GREEN + "Regestered Sign as " + ChatColor.DARK_GREEN + "ID " + TesterSign.getTSByLocation(b.getLocation()).getID() + ChatColor.GREEN + " under map " + ChatColor.YELLOW + map.getName() + ".");
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.map.settester");
							p.getInventory().setItemInMainHand(new ItemStack(Material.AIR, 1));
						}
					}
				}
			}
			if(p.getInventory().getItemInMainHand().getType() == Material.getMaterial(101)) {
				for(Map map : Map.maps) {
					if(p.getInventory().getItemInMainHand().isSimilar(ItemManagement.getBarsItem(map))) {
						if(p.hasPermission("sabotage.map.settester")) {
							IronBar.createIronBar(map, b.getX(), b.getY(), b.getZ());
							p.sendMessage(ChatColor.GREEN + "Regestered Iron Bar as " + ChatColor.DARK_GREEN + "ID " + IronBar.getIronBarByLocation(b.getLocation()).getID() + ChatColor.GREEN + " under map " + ChatColor.YELLOW + map.getName() + ".");
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.map.settester");
							p.getInventory().setItemInMainHand(new ItemStack(Material.AIR, 1));
						}
					}
				}
			}
			if(p.getInventory().getItemInMainHand().getType() == Material.WOOL) {
				for(Map map : Map.maps) {
					if(p.getInventory().getItemInMainHand().isSimilar(ItemManagement.getWoolItem(map))) {
						if(p.hasPermission("sabotage.map.settester")) {
							StatusWool.createSW(map, b.getX(), b.getY(), b.getZ());
							p.sendMessage(ChatColor.GREEN + "Regestered Status Wool as " + ChatColor.DARK_GREEN + "ID " + StatusWool.getSWByLocation(b.getLocation()).getID() + ChatColor.GREEN + " under map " + ChatColor.YELLOW + map.getName() + ".");
						} else {
							p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.map.settester");
							p.getInventory().setItemInMainHand(new ItemStack(Material.AIR, 1));
						}
					}
				}
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerBreakBlock(BlockBreakEvent e) {
		Player p = e.getPlayer();
		Block b = e.getBlock();
		if(knc && !playersPlaying.contains(p)) {
			if(!Rank.activePlayerRanks(p.getName()).contains(Rank.ADMIN) && !Rank.activePlayerRanks(p.getName()).contains(Rank.OWNER)) {
				e.setCancelled(true);
			}
		}
		if(playersPlaying.contains(p)) {
			if(gamePhase == 0 || gamePhase == 1 || gamePhase == 2) {
				e.setCancelled(true);
			}
		}
		if(b.getType() == Material.CHEST || b.getType() == Material.ENDER_CHEST) {
			if(Crate.crates != null) {
				for(Crate crate : Crate.crates) {
					if(crate.getX() == b.getX() && crate.getY() == b.getY() && crate.getZ() == b.getZ()) {
						if(p.hasPermission("sabotage.map.setcrates")) {
							p.sendMessage(ChatColor.RED + "Unregestered Crate " + ChatColor.DARK_RED + "ID " + Crate.getCrateByLocation(b.getLocation()).getID() + ChatColor.RED + " for map " + ChatColor.YELLOW + Crate.getCrateByLocation(b.getLocation()).getMap().getName() + ".");
							Crate.removeCrate(b.getLocation());
						} else {
							e.setCancelled(true);
							if(!playersPlaying.contains(p)) {
								p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.map.setcrates");
							}
						}
					}
				}
			}
		}
		if(b.getType() == Material.SIGN || b.getType() == Material.SIGN_POST || b.getType() == Material.WALL_SIGN) {
			if(TesterSign.TSs != null) {
				for(TesterSign TS : TesterSign.TSs) {
					if(TS.getX() == b.getX() && TS.getY() == b.getY() && TS.getZ() == b.getZ()) {
						if(p.hasPermission("sabotage.map.settester")) {
							p.sendMessage(ChatColor.RED + "Unregestered Sign " + ChatColor.DARK_RED + "ID " + TesterSign.getTSByLocation(b.getLocation()).getID() + ChatColor.RED + " for map " + ChatColor.YELLOW + TesterSign.getTSByLocation(b.getLocation()).getMap().getName() + ".");
							TesterSign.removeTS(b.getLocation());
						} else {
							e.setCancelled(true);
							if(!playersPlaying.contains(p)) {
								p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.map.settester");
							}
						}
					}
				}
			}
		}
		if(b.getType() == Material.getMaterial(101)) {
			if(IronBar.ironbars != null) {
				for(IronBar ib : IronBar.ironbars) {
					if(ib.getX() == b.getX() && ib.getY() == b.getY() && ib.getZ() == b.getZ()) {
						if(p.hasPermission("sabotage.map.settester")) {
							p.sendMessage(ChatColor.RED + "Unregestered Iron Bar " + ChatColor.DARK_RED + "ID " + IronBar.getIronBarByLocation(b.getLocation()).getID() + ChatColor.RED + " for map " + ChatColor.YELLOW + IronBar.getIronBarByLocation(b.getLocation()).getMap().getName() + ".");
							IronBar.removeIronBar(b.getLocation());
						} else {
							e.setCancelled(true);
							if(!playersPlaying.contains(p)) {
								p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.map.settester");
							}
						}
					}
				}
			}
		}
		if(b.getType() == Material.WOOL) {
			if(StatusWool.SWs != null) {
				for(StatusWool SW : StatusWool.SWs) {
					if(SW.getX() == b.getX() && SW.getY() == b.getY() && SW.getZ() == b.getZ()) {
						if(p.hasPermission("sabotage.map.settester")) {
							p.sendMessage(ChatColor.RED + "Unregestered Status Wool " + ChatColor.DARK_RED + "ID " + StatusWool.getSWByLocation(b.getLocation()).getID() + ChatColor.RED + " for map " + ChatColor.YELLOW + StatusWool.getSWByLocation(b.getLocation()).getMap().getName() + ".");
							StatusWool.removeSW(b.getLocation());
						} else {
							e.setCancelled(true);
							if(!playersPlaying.contains(p)) {
								p.sendMessage(ChatColor.RED + "Missing Permission " + ChatColor.DARK_RED + "sabotage.map.settester");
							}
						}
					}
				}
			}
		}
	}
	
	@EventHandler
	public void onPlayerDropItem(PlayerDropItemEvent e) {
		Player p = e.getPlayer();
		if(playersPlaying.contains(p)) {
			if(gamePhase == 0) {
				e.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onPlayerGameModeChange(PlayerGameModeChangeEvent e) {
		Player p = e.getPlayer();
		if(playersPlaying.contains(p)) {
			if(gamePhase == 1 || gamePhase == 2) {
				for(Player pl : playersPlaying) {
					if(Role.playersRole.containsKey(pl)) {
						pl.setScoreboard(Role.playersRole.get(pl).generateScoreboard(pl));
					}
				}
				if(e.getNewGameMode() == GameMode.SPECTATOR && p.getGameMode() != GameMode.SPECTATOR) {
					SABTools.sendGameMessage(ChatColor.DARK_RED + "A player has died...");
					SABTools.sendGameMessage(ChatColor.AQUA + "" + (SABTools.playersAlive() - 1) + " players remain!");
					SABTools.sendGameSound(Sound.ENTITY_LIGHTNING_THUNDER, 5F, 1F);
					if(Bukkit.getPluginManager().getPlugin("kNoCore") != null) {
						Cosmetic.deactivateAll(p);
					}
					return;
				}
				if(e.getNewGameMode() != GameMode.SPECTATOR && p.getGameMode() == GameMode.SPECTATOR) {
					SABTools.sendGameMessage(ChatColor.DARK_GREEN + "A player has respawned...");
					SABTools.sendGameMessage(ChatColor.GRAY + "" + (SABTools.playersAlive() + 1) + " players remain!");
					SABTools.sendGameSound(Sound.ENTITY_WITHER_SPAWN, 5F, 1F);
					p.setInvulnerable(false);
					p.setCanPickupItems(true);
					return;
				}
			}
			p.setLevel(Karma.getKarma(p.getName()));
		}
	}
	
	@EventHandler
	public void onPlayerDamage(EntityDamageByEntityEvent e) {
		if(e.getEntity() instanceof Player) {
			Player p = (Player) e.getEntity();
			if(e.getDamager() instanceof Player) {
				Player d = (Player) e.getDamager();
				if((!playersPlaying.contains(p) && playersPlaying.contains(d)) || (playersPlaying.contains(p) && !playersPlaying.contains(d))) {
					e.setCancelled(true);
				}
			}
		}
	}
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent e) {
		if(e.getEntity() instanceof Player) {
			Player p = (Player) e.getEntity();
			if(playersPlaying.contains(p) && p.getGameMode() == GameMode.SPECTATOR) {
				if(gamePhase == 1 || gamePhase == 2 || gamePhase == 3) {
					e.setDeathMessage("");
					p.setHealth(20);
					return;
				}
			}
			if(playersPlaying.contains(p) && p.getGameMode() != GameMode.SPECTATOR) {
				if(gamePhase == 0) {
					p.setHealth(20);
					return;
				}
				if(playersWithHealthBenefits.contains(p)) {
					p.setHealth(SABTools.randomNumber(8, 12));
					p.sendMessage(ChatColor.GOLD + "Second Wind was used!");
					playersWithHealthBenefits.remove(p);
					return;
				}
				e.setDeathMessage("");
				p.setHealth(20);
				p.setGameMode(GameMode.SPECTATOR);
				if(playersWithBigBoom.contains(p)) {
					p.sendMessage(ChatColor.DARK_RED + "You dropped a Martyr!");
					playersWithBigBoom.remove(p);
					TNTPrimed tnt = p.getWorld().spawn(p.getLocation(), TNTPrimed.class);
					tnt.setYield(3F);
					tnt.setFuseTicks(20);
					tnt.setIsIncendiary(true);
				}
				this.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
		    		public void run() {
						if(Role.playersRole.get(p) == Role.INNOCENT) {
							p.sendMessage(ChatColor.GRAY + "You have died.");
						}
						if(Role.playersRole.get(p) == Role.DETECTIVE) {
							p.sendMessage(ChatColor.BLUE + "You failed to be a successful Detective.");
						}
						if(Role.playersRole.get(p) == Role.SABOTEUR) {
							p.sendMessage(ChatColor.DARK_RED + "You failed to be a successful Saboteur.");
						}
						
						if(Data.MAIN.getFileConfig().getInt("Karma.Values." + Role.playersRole.get(p).getName() + ".Death") > 0) {
							p.sendMessage(ChatColor.GREEN + "You've gained " + ChatColor.DARK_GREEN + Data.MAIN.getFileConfig().getInt("Karma.Values." + Role.playersRole.get(p).getName() + ".Death") + ChatColor.GREEN + " Karma!");
							Karma.addKarma(p.getName(), Data.MAIN.getFileConfig().getInt("Karma.Values." + Role.playersRole.get(p).getName() + ".Death"));
						} else if(Data.MAIN.getFileConfig().getInt("Karma.Values." + Role.playersRole.get(p).getName() + ".Death") < 0) {
							p.sendMessage(ChatColor.RED + "You've lost " + ChatColor.DARK_RED + (-1 * Data.MAIN.getFileConfig().getInt("Karma.Values." + Role.playersRole.get(p).getName() + ".Death")) + ChatColor.RED + " Karma.");
							Karma.addKarma(p.getName(), Data.MAIN.getFileConfig().getInt("Karma.Values." + Role.playersRole.get(p).getName() + ".Death"));
						} else {
							p.sendMessage(ChatColor.GRAY + "Your Karma was uneffected.");
						}
						Karma.updateKarma(p);
						if(e.getEntity().getKiller() != null) {
							if(e.getEntity().getKiller() instanceof Player) {
								Player k = (Player) e.getEntity().getKiller();
								timeSinceLastKill.put(k, 0);
								String role = ChatColor.AQUA + p.getName() + " was a " + Role.playersRole.get(p).getColor() + Role.playersRole.get(p).getName() + ChatColor.AQUA + "! ";
								if(Data.MAIN.getFileConfig().getInt("Karma.Values." + Role.playersRole.get(k).getName() + ".Kill." + Role.playersRole.get(p).getName()) > 0) {
									k.sendMessage(role + ChatColor.GREEN + "You've gained " + ChatColor.DARK_GREEN + Data.MAIN.getFileConfig().getInt("Karma.Values." + Role.playersRole.get(k).getName() + ".Kill." + Role.playersRole.get(p).getName()) + ChatColor.GREEN + " Karma!");
									Karma.addKarma(k.getName(), Data.MAIN.getFileConfig().getInt("Karma.Values." + Role.playersRole.get(k).getName() + ".Kill." + Role.playersRole.get(p).getName()));
								} else if(Data.MAIN.getFileConfig().getInt("Karma.Values." + Role.playersRole.get(k).getName() + ".Kill." + Role.playersRole.get(p).getName()) < 0) {
									k.sendMessage(role + ChatColor.RED + "You've lost " + ChatColor.DARK_RED + (-1 * Data.MAIN.getFileConfig().getInt("Karma.Values." + Role.playersRole.get(k).getName() + ".Kill." + Role.playersRole.get(p).getName())) + ChatColor.RED + " Karma.");
									Karma.addKarma(k.getName(), Data.MAIN.getFileConfig().getInt("Karma.Values." + Role.playersRole.get(k).getName() + ".Kill." + Role.playersRole.get(p).getName()));
								} else {
									k.sendMessage(role + ChatColor.GRAY + "Your Karma was uneffected.");
								}
								Karma.updateKarma(k);
							}
						}
		    		}
				}, 1L);
			}
		}
	}
	
	@EventHandler
	public void onPlayerClickPlayer(PlayerInteractAtEntityEvent e) {
		Player p = e.getPlayer();
		if(e.getRightClicked() instanceof Player) {
			Player c = (Player) e.getRightClicked();
			if(playersPlaying.contains(p) && p.getGameMode() != GameMode.SPECTATOR && playersPlaying.contains(c) && c.getGameMode() != GameMode.SPECTATOR) {
				if(e.getHand() == EquipmentSlot.HAND) {
					if(p.getInventory().getItemInMainHand() != null) {
						if(p.getInventory().getItemInMainHand().isSimilar(ItemManagement.getInspectorItem())) {
							if(gamePhase != 2) {
								p.sendMessage(ChatColor.RED + "You cannot use this yet!");
								p.playSound(p.getLocation(), Sound.ENTITY_CHICKEN_HURT, 5F, 1F);
								return;
							}
							if(ItemManagement.inspecterCooldown.containsKey(p)) {
								p.sendMessage(ChatColor.RED + "You cannot use this item for another " + ChatColor.DARK_RED + ItemManagement.inspecterCooldown.get(p) + ChatColor.RED + " seconds!");
								return;
							} else {
								ItemManagement.inspecterCooldown.put(p, Data.MAIN.getFileConfig().getInt("Items.InspecterGlassCooldown"));
								ItemManagement.runInspectorItemCooldown(p);
							}
							if(detectiveInsight.contains(p)) {
								detectiveInsight.remove(p);
								p.sendMessage(ChatColor.BLUE + "Insight shows this player to be a " + Role.playersRole.get(c).getColor() + Role.playersRole.get(c).getName() + ChatColor.BLUE + "!");
								return;
							}
							int random = SABTools.randomNumber(0, 2);
							if(timeSinceLastKill.containsKey(c)) {
								if(timeSinceLastKill.get(c) <= 20) {
									if(random == 0) {
										p.sendMessage(ChatColor.GRAY + "He's bruised pretty bad. Was he just in a fight?!?");
									}
									if(random == 1) {
										p.sendMessage(ChatColor.GRAY + "They're completely drenched in blood! Did they just kill someone?!?");
									}
									if(random == 2) {
										p.sendMessage(ChatColor.GRAY + "Blood drips from their sword... It was just used..!");
									}
								} else if(timeSinceLastKill.get(c) <= 40) {
									if(random == 0) {
										p.sendMessage(ChatColor.GRAY + "They seem out of breath... Why?");
									}
									if(random == 1) {
										p.sendMessage(ChatColor.GRAY + "So much blood... it's dried though, I bet they killed someone...");
									}
									if(random == 2) {
										p.sendMessage(ChatColor.GRAY + "Hmm, there's a few dried blood stains on their body...");
									}
								} else if(timeSinceLastKill.get(c) <= 60) {
									if(random == 0) {
										p.sendMessage(ChatColor.GRAY + "Everthing checks out... except the red shirt stain...");
									}
									if(random == 1) {
										p.sendMessage(ChatColor.GRAY + "Odd... He looks clean. Possibly too clean?");
									}
									if(random == 2) {
										p.sendMessage(ChatColor.GRAY + "Ah! there's a few fresh blood stains on their body..!");
									}
								}
							} else {
								if(random == 0) {
									p.sendMessage(ChatColor.GRAY + "Odd... He looks clean. Possibly too clean...");
								}
								if(random == 1) {
									p.sendMessage(ChatColor.GRAY + "Ah! there's a few fresh blood stains on their body..!");
								}
								if(random == 2) {
									p.sendMessage(ChatColor.GRAY + "Hmm, there's a few dried blood stains on their body...");
								}
							}
						}
					}
				}
			}
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerChat(AsyncPlayerChatEvent e) {
		Player p = e.getPlayer();
		//String msg = e.getMessage();
		String f = e.getFormat();
		
		if(playersPlaying.size() > 0 && Data.MAIN.getFileConfig().getBoolean("General.SeparatedChat") == true && (gamePhase == 1 || gamePhase == 2 || gamePhase == 3)) {
			if(playersPlaying.contains(p)) {
				e.setCancelled(true);
				f = f.toString().replace("%%", "%");
				if(p.getGameMode() == GameMode.SPECTATOR) {
					SABTools.sendDeadGameMessage(ChatColor.GRAY + "" + ChatColor.ITALIC + "Dead " + ChatColor.RESET + f);
				} else {
					if(Data.MAIN.getFileConfig().getBoolean("General.SendMessagesToConsole") == true) {
						Bukkit.getConsoleSender().sendMessage(ChatColor.YELLOW + "[" + ChatColor.GOLD + "S" + ChatColor.YELLOW + "] " + ChatColor.WHITE + f);
					}
					for(Player pl : Sabotage.playersPlaying) {
						if(Role.playersRole.get(p) == Role.SABOTEUR && Role.playersRole.get(pl) == Role.SABOTEUR) {
							pl.sendMessage(ChatColor.WHITE + "[" + ChatColor.RED + "S" + ChatColor.WHITE + "] " + f);
						} else if(Role.playersRole.get(p) == Role.DETECTIVE){
							pl.sendMessage(ChatColor.BLUE + "[" + ChatColor.AQUA + "D" + ChatColor.BLUE + "] " + ChatColor.WHITE + f);
						} else {
							pl.sendMessage(f);
						}
					}
				}
			} else {
				for(Player pl : playersPlaying) {
					e.getRecipients().remove(pl);
				}
				/*
				for(Player pl : Bukkit.getOnlinePlayers()) {
					if(!playersPlaying.contains(pl)) {
						pl.sendMessage(f);
						Bukkit.getConsoleSender().sendMessage(f);
					}
				}
				*/
			}
		}
	}
	
	@EventHandler
	public void onFoodChange(FoodLevelChangeEvent e) {
		if(e.getEntity() instanceof Player) {
			Player p = (Player) e.getEntity();
			if(playersPlaying.contains(p)) {
				if(gamePhase == 0) {
					e.setFoodLevel(20);
				} else {
					e.setFoodLevel(16);
				}
			}
		}
	}
	
	@EventHandler
	public void onPlayerTeleport(PlayerTeleportEvent e) {
		Player p = e.getPlayer();
		if(playersPlaying.contains(p) && gamePhase > 0 && e.getCause() == TeleportCause.SPECTATE) {
			if(e.getTo().getWorld() != Map.currentlySelectedMap.getSpawnLocation().getWorld()) {
				e.setCancelled(true);
				p.sendMessage(ChatColor.RED + "You cannot teleport there!");
				p.playSound(p.getLocation(), Sound.ENTITY_CHICKEN_HURT, 5F, 1F);
				return;
			}
			if(e.getTo().distance(Map.currentlySelectedMap.getSpawnLocation()) < Data.MAIN.getFileConfig().getInt("World.SpectatorMapRange")) {
				e.setCancelled(true);
				p.sendMessage(ChatColor.RED + "You cannot teleport there!");
				p.playSound(p.getLocation(), Sound.ENTITY_CHICKEN_HURT, 5F, 1F);
				return;
			}
		}
	}
}
