package com.sabotage.kNoAPP;

import java.util.ArrayList;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;

public class StatusWool {
	public static ArrayList<StatusWool> SWs = new ArrayList<StatusWool>();
	
	private Map map;
	private int id;
	private int x;
	private int y;
	private int z;

	public StatusWool(Map map, int id, int x, int y, int z) {
		this.map = map;
		this.id = id;
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public static void createSW(Map map, int x, int y, int z) {
		FileConfiguration fc = Data.MAIN.getFileConfig();
		int i = getNextOpenID(map);
		fc.set("Maps.MapIDs." + map.getID() + ".SWs.SWIDs." + i + ".X", x);
		fc.set("Maps.MapIDs." + map.getID() + ".SWs.SWIDs." + i + ".Y", y);
		fc.set("Maps.MapIDs." + map.getID() + ".SWs.SWIDs." + i + ".Z", z);
		fc.set("Maps.MapIDs." + map.getID() + ".SWs.SWCount", fc.getInt("Maps.MapIDs." + map.getID() + ".SWs.SWCount") + 1);
		Data.MAIN.saveDataFile(fc);
		refreshSWs();
	}
	
	public static void removeSW(Location l) {
		FileConfiguration fc = Data.MAIN.getFileConfig();
		for(StatusWool SW : SWs) {
			if(SABTools.compareLocationXYZ(SW.getX(), SW.getY(), SW.getZ(), l)) {
				fc.set("Maps.MapIDs." + SW.getMap().getID() + ".SWs.SWIDs." + SW.getID(), null);
				fc.set("Maps.MapIDs." + SW.getMap().getID() + ".SWs.SWCount", fc.getInt("Maps.MapIDs." + SW.getMap().getID() + ".SWs.SWCount") - 1);
			}
		}
		Data.MAIN.saveDataFile(fc);
		refreshSWs();
	}
	
	public static StatusWool getSWByLocation(Location l) {
		for(StatusWool SW : SWs) {
			if(SABTools.compareLocationXYZ(SW.getX(), SW.getY(), SW.getZ(), l)) {
				return SW;
			}
		}
		return null;
	}
	
	public static boolean validSW(Location l) {
		int ax = l.getBlockX();
		int ay = l.getBlockY();
		int az = l.getBlockZ();
		for(StatusWool SW : SWs) {
			if(Data.MAIN.getFileConfig().getDouble("Maps.MapIDs." + SW.getMap().getID() + ".SWs.SWIDs." + SW.getID() + ".X") == ax && Data.MAIN.getFileConfig().getDouble("Maps.MapIDs." + SW.getMap().getID() + ".SW.SWIDs." + SW.getID() + ".Y") == ay && Data.MAIN.getFileConfig().getDouble("Maps.MapIDs." + SW.getMap().getID() + ".SW.SWIDs." + SW.getID() + ".Z") == az) {
				return true;
			}
		}
		return false;
	}
	
	public static int getNextOpenID(Map map) {
		int currentID = 1;
		while(currentID < 10000) {
			if(!Data.MAIN.getFileConfig().isSet("Maps.MapIDs." + map.getID() + ".SWs.SWIDs." + currentID)) {
				return currentID;
			}
			currentID++;
		}
		return 0;
	}
	
	public Map getMap() {
		return map;
	}
	
	public int getID() {
		return id;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public int getZ() {
		return z;
	}
	
	public static void refreshSWs() {
		SWs = importSWs(Data.MAIN.getFileConfig());
	}
	
	public static ArrayList<StatusWool> importSWs(FileConfiguration fc) {
		ArrayList<StatusWool> SWs = new ArrayList<StatusWool>();
		for(Map map : Map.maps) {
			int swc = fc.getInt("Maps.MapIDs." + map.getID() + ".SWs.SWCount");
			for(int i=1; i<=swc; i++) {
				if(!fc.isSet("Maps.MapIDs." + map.getID() + ".SWs.SWIDs." + i)) {
					swc++;
					continue;
				}
				int x = 0;
				int y = 0;
				int z = 0;
				if(fc.isSet("Maps.MapIDs." + map.getID() + ".SWs.SWIDs." + i + ".X")) {
					x = fc.getInt("Maps.MapIDs." + map.getID() + ".SWs.SWIDs." + i + ".X");
				}
				if(fc.isSet("Maps.MapIDs." + map.getID() + ".SWs.SWIDs." + i + ".Y")) {
					y = fc.getInt("Maps.MapIDs." + map.getID() + ".SWs.SWIDs." + i + ".Y");
				}
				if(fc.isSet("Maps.MapIDs." + map.getID() + ".SWs.SWIDs." + i + ".Z")) {
					z = fc.getInt("Maps.MapIDs." + map.getID() + ".SWs.SWIDs." + i + ".Z");
				}
				
				StatusWool SW = new StatusWool(map, i, x, y, z);
				SWs.add(SW);
			}
		}
		return SWs;
	}
	
	public static void loadSW(Map m) {
		for(StatusWool sw : SWs) {
			if(sw.getMap() == m) {
				sw.getMap().getSpawnLocation().getWorld().getBlockAt(sw.getX(), sw.getY(), sw.getZ()).setType(Material.WOOL);
			}
		}
	}
}
