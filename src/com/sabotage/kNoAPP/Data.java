package com.sabotage.kNoAPP;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

public class Data {
	public static final Data CONFIG = new Data(new File(Bukkit.getPluginManager().getPlugin("Sabotage").getDataFolder(), "config.yml"), "config.yml");
	public static final Data MAIN = new Data(null, "main.yml");
	public static final Data PLAYER = new Data(null, "player.yml");
	
	public static Data[] configs = new Data[]{CONFIG, MAIN, PLAYER}; 

	public static Plugin plugin = Bukkit.getPluginManager().getPlugin("Sabotage");
	
	private File file;
	private String fileName;
	
	public Data(File file, String fileName) {
		this.file = file;
		this.fileName = fileName;
	}
	
	public File getFile() {
		return file;
	}
	
	public void setFile(String path) {
		if(path == "") {
			this.file = new File(Bukkit.getPluginManager().getPlugin("Sabotage").getDataFolder(), this.getFileName());
		} else {
			this.file = new File(path, this.getFileName());
		}
	}
	
	public FileConfiguration getFileConfig() {
		FileConfiguration fc = new YamlConfiguration(); 
		try {
			fc.load(this.getFile());
		 } catch (FileNotFoundException e) {
			 e.printStackTrace();
	     } catch (IOException e) {
	         e.printStackTrace();
	     } catch (InvalidConfigurationException e) {
	    	 e.printStackTrace();
	     }
		return fc;
	}
	
	public String getFileName() {
		return fileName;
	}
	
	public String getPath() {
		return this.getFile().getAbsolutePath();
	}
	
	public void createDataFile() {
		if(!this.getFile().exists()) {
			plugin.getLogger().info(this.getFileName() + " not found. Creating...");
			try {
				this.getFile().createNewFile();
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			FileConfiguration fc = null;
			fc = this.getFileConfig();
			if(this == CONFIG) {
				fc.set("UseMainFolder", true);
				fc.set("UseCustomFolder", "/example/path/");
			}
			if(this == MAIN) {
				fc.set("Version", "1.0.0");
		        
		        fc.set("General.NewMaps.EnableByDefault", false);
		        fc.set("General.GameOptions.SafeRunningTime", 30);
		        fc.set("General.SeparatedChat", true);
		        fc.set("General.SendMessagesToConsole", true);
		        fc.set("General.RestartServerOnFinish", false);
		        fc.set("General.BanOnZeroKarma", true);
		        
		        fc.set("Items.InspecterGlassCooldown", 10);
		        
		        fc.set("World.Locations.Lobby", null);
		        fc.set("World.Crates.SpawnPercentEachRound", 75);
		        fc.set("World.Crates.RespawnEveryXSeconds", 300);
		        fc.set("World.SpectatorMapRange", 150);
		        fc.set("World.EnableTNTLandDamage", false);
		        
		        fc.set("Lobby.Time.MAXStartTime", 60);
		        fc.set("Lobby.Time.MINStartTime", 30);
		        fc.set("Lobby.PlayersNeeded", 10);
		        fc.set("Lobby.Voting", true);
		        
		        fc.set("Karma.DailyReward", 25);
		        fc.set("Karma.Values.Defaults.StartingBal", 250);
		        
		        fc.set("Karma.Values.Innocent.Kill.Innocent", -50);
		        fc.set("Karma.Values.Innocent.Kill.Detective", -75);
		        fc.set("Karma.Values.Innocent.Kill.Saboteur", 40);
		        fc.set("Karma.Values.Innocent.WinGame", 40);
		        fc.set("Karma.Values.Innocent.LoseGame", 0);
		        fc.set("Karma.Values.Innocent.Death", 0);
		        
		        fc.set("Karma.Values.Detective.Kill.Innocent", -25);
		        fc.set("Karma.Values.Detective.Kill.Saboteur", 20);
		        fc.set("Karma.Values.Detective.WinGame", 80);
		        fc.set("Karma.Values.Detective.LoseGame", 0);
		        fc.set("Karma.Values.Detective.Death", 0);
		        
		        fc.set("Karma.Values.Saboteur.Kill.Innocent", 25);
		        fc.set("Karma.Values.Saboteur.Kill.Detective", 40);
		        fc.set("Karma.Values.Saboteur.Kill.Saboteur", -75);
		        fc.set("Karma.Values.Saboteur.WinGame", 40);
		        fc.set("Karma.Values.Saboteur.LoseGame", 0);
		        fc.set("Karma.Values.Saboteur.Death", -25);
		        
		        fc.set("Maps.MapCount", 0);
			}
			this.saveDataFile(fc);
        } else {
        	plugin.getLogger().info(this.getFileName() + " found. Loading...");
        	if(this == MAIN) {
        		FileConfiguration fc = this.getFileConfig();
        		if(fc.getString("Version").equals("1.0.0")) {
					fc.set("Version", "1.0.1");
					fc.set("General.GameOptions.OneSabForEveryXPlayers", 3);
				}
        		this.saveDataFile(fc);
        	}
        }
	}
	
	public void saveDataFile(FileConfiguration fc) {
		try {
			fc.save(this.getFile());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
