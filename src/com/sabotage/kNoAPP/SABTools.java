package com.sabotage.kNoAPP;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.knc.kNoAPP.Rank;

public class SABTools {
	
	public static void onJoin(Player p) {
		if(Data.MAIN.getFileConfig().get("World.Locations.Lobby") == null) {
			p.sendMessage(ChatColor.DARK_RED + "No lobby spawn for Sabotage has been set!");
			p.sendMessage(ChatColor.RED + "Contact an admin to get the lobby set!");
			return;
		}
		FileConfiguration fc = Data.PLAYER.getFileConfig();
		if(!Sabotage.playersPlaying.contains(p)) {
			fc.set("Player." + p.getUniqueId() + ".Name", p.getName());
			fc.set("Player." + p.getUniqueId() + ".Joins", fc.getInt("Player." + p.getUniqueId() + ".Joins") + 1);
			Data.PLAYER.saveDataFile(fc);
			if(fc.getInt("Player." + p.getUniqueId() + ".Joins") == 1) {
				fc.set("Player." + p.getUniqueId()+ ".Karma", Data.MAIN.getFileConfig().getInt("Karma.Values.Defaults.StartingBal"));
				fc.set("Player." + p.getUniqueId()+ ".SaboteurPasses", 0);
			}
			Data.PLAYER.saveDataFile(fc);
			if(Karma.getKarma(p.getName()) <= 0 && Data.MAIN.getFileConfig().getBoolean("General.BanOnZeroKarma") == true) {
				p.sendMessage(ChatColor.DARK_RED + "You do not have enough Karma to play! Come back later.");
				p.playSound(p.getLocation(), Sound.ENTITY_CHICKEN_HURT, 2F, 1F);
				return;
			}
			Sabotage.playersPlaying.add(p);
			Sabotage.savedInventory.put(p, p.getInventory().getStorageContents());
			Sabotage.savedArmorInventory.put(p, p.getInventory().getArmorContents());
			Sabotage.savedTotalExperiance.put(p, p.getLevel());
			Sabotage.savedLocation.put(p, p.getLocation());
			Sabotage.savedGamemode.put(p, p.getGameMode());
			SABTools.clearFullInv(p);
		
			if(Sabotage.gamePhase == 0) {
				Karma.updateKarma(p);
				p.setFoodLevel(20);
				p.setHealth(20);
				p.teleport((Location) Data.MAIN.getFileConfig().get("World.Locations.Lobby"));
				p.setGameMode(GameMode.SURVIVAL);
				p.setInvulnerable(true);
				p.setCanPickupItems(false);
				if(Sabotage.playersPlaying.size() == 1) {
					Schedule.runSchedule(0, Data.MAIN.getFileConfig().getInt("Lobby.Time.MAXStartTime"), 20L);
				}
				SABTools.sendGameMessage(ChatColor.GREEN + p.getName() + ChatColor.GRAY + " joined the game!");
				SABTools.sendGameSound(Sound.BLOCK_CHEST_OPEN, 2F, 1F);
				if(Bukkit.getPluginManager().getPlugin("kNoCore") != null) {
					if(Rank.inheritsRank(p, Rank.SILVER)) {
						if(Passes.getSabPasses(p.getName()) != 1) {
							Passes.addSabPasses(p.getName(), 1 - Passes.getSabPasses(p.getName()));
						}
					}
				}
				p.getInventory().setItem(0, ItemManagement.getMapSelector());
				p.getInventory().setItem(4, ItemManagement.getKarmaCount(p));
				if(Data.PLAYER.getFileConfig().getInt("Player." + p.getUniqueId() + ".SaboteurPasses") > 0) {
					if(Sabotage.playersWithPasses.contains(p)) {
						p.getInventory().setItem(8, ItemManagement.getSabPass(true, Data.PLAYER.getFileConfig().getInt("Player." + p.getUniqueId() + ".SaboteurPasses")));
					} else {
						p.getInventory().setItem(8, ItemManagement.getSabPass(false, Data.PLAYER.getFileConfig().getInt("Player." + p.getUniqueId() + ".SaboteurPasses")));
					}
				} else {
					p.getInventory().setItem(8, ItemManagement.getNoSabPass());
				}
			} else if(Sabotage.gamePhase == 1 || Sabotage.gamePhase == 2 || Sabotage.gamePhase == 3) {
				if(!Role.playersRole.containsKey(p)) {
					Role.playersRole.put(p, Role.INNOCENT);
				}
				p.teleport(Map.currentlySelectedMap.getSpawnLocation());
				p.setInvulnerable(true);
				p.setCanPickupItems(false);
				SABTools.sendGameMessage(ChatColor.GRAY + p.getName() + ChatColor.DARK_GRAY + " joined as a spectator!");
				for(Player pl : Sabotage.playersPlaying) {
					if(Role.playersRole.containsKey(pl)) {
						pl.setScoreboard(Role.playersRole.get(pl).generateScoreboard(pl));
					}
				}
				Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Bukkit.getPluginManager().getPlugin("Sabotage"), new Runnable() {
		    		public void run() {
		    			p.setGameMode(GameMode.SPECTATOR);
		    		}
				}, 2L);
			}
		} else {
			p.sendMessage(ChatColor.RED + "You are already playing Sabotage!");
		}
	}
	
	public static void onLeave(Player p, boolean silent) {
		if(Sabotage.playersPlaying.contains(p)) {
			Sabotage.playersPlaying.remove(p);
			p.teleport(Sabotage.savedLocation.get(p));
			SABTools.clearFullInv(p);
			p.setInvulnerable(false);
			p.setCanPickupItems(true);
			p.getActivePotionEffects().clear();
			for(ItemStack is : Sabotage.savedInventory.get(p)) {
				if(is != null) {
					p.getInventory().addItem(is);
				}
			}
			p.getInventory().setArmorContents(Sabotage.savedArmorInventory.get(p));
			p.setLevel(Sabotage.savedTotalExperiance.get(p));
			p.setGameMode(Sabotage.savedGamemode.get(p));
			p.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
			
			if(Sabotage.gamePhase == 0) {
				SABTools.sendGameMessage(ChatColor.RED + p.getName() + ChatColor.GRAY + " left the game!");
				SABTools.sendGameSound(Sound.BLOCK_CHEST_CLOSE, 2F, 1F);
			}
			if(Sabotage.gamePhase == 1 || Sabotage.gamePhase == 2) {
				SABTools.sendGameMessage(ChatColor.AQUA + "A player left the game...");
				SABTools.sendGameMessage(ChatColor.BLUE + "" + playersAlive() + " players remain.");
				for(Player pl : Sabotage.playersPlaying) {
					if(Role.playersRole.containsKey(pl)) {
						pl.setScoreboard(Role.playersRole.get(pl).generateScoreboard(pl));
					}
				}
			}
		} else {
			if(!silent) {
				p.sendMessage(ChatColor.RED + "You aren't playing Sabotage!");
			}
		}
	}
	
	public static void sendGameMessage(String s) {
		if(Data.MAIN.getFileConfig().getBoolean("General.SendMessagesToConsole") == true) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.YELLOW + "[" + ChatColor.GOLD + "S" + ChatColor.YELLOW + "] " + ChatColor.WHITE + s);
		}
		for(Player pl : Sabotage.playersPlaying) {
			pl.sendMessage(s);
		}
	}
	
	public static void sendDeadGameMessage(String s) {
		if(Data.MAIN.getFileConfig().getBoolean("General.SendMessagesToConsole") == true) {
			Bukkit.getConsoleSender().sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GRAY + "S" + ChatColor.DARK_GRAY + "] " + ChatColor.WHITE + s);
		}
		for(Player pl : Sabotage.playersPlaying) {
			if(pl.getGameMode() == GameMode.SPECTATOR) {
				pl.sendMessage(s);
			}
		}
	}
	
	public static void sendGameSound(Sound s, float v, float p) {
		for(Player pl : Bukkit.getOnlinePlayers()) {
			if(Sabotage.playersPlaying.contains(pl)) {
				pl.playSound(pl.getLocation(), s, v, p);
			}
		}
	}
	
	public static void clearFullInv(Player p) {
		p.getInventory().clear();
		p.getInventory().setBoots(new ItemStack(Material.AIR, 1));
		p.getInventory().setLeggings(new ItemStack(Material.AIR, 1));
		p.getInventory().setChestplate(new ItemStack(Material.AIR, 1));
		p.getInventory().setHelmet(new ItemStack(Material.AIR, 1));
		return;
	}
	
	public static ItemStack generateDisc(int a, Map map) {
		ItemStack is = null;
		if(a % 10 == 0) {
			is = new ItemStack(Material.GREEN_RECORD, MapVoting.mapVotes.get(map));
		}
		if(a % 10 == 1) {
			is = new ItemStack(Material.GOLD_RECORD, MapVoting.mapVotes.get(map));
		}
		if(a % 10 == 2) {
			is = new ItemStack(Material.RECORD_10, MapVoting.mapVotes.get(map));
		}
		if(a % 10 == 3) {
			is = new ItemStack(Material.RECORD_9, MapVoting.mapVotes.get(map));
		}
		if(a % 10 == 4) {
			is = new ItemStack(Material.RECORD_8, MapVoting.mapVotes.get(map));
		}
		if(a % 10 == 5) {
			is = new ItemStack(Material.RECORD_7, MapVoting.mapVotes.get(map));
		}
		if(a % 10 == 6) {
			is = new ItemStack(Material.RECORD_6, MapVoting.mapVotes.get(map));
		}
		if(a % 10 == 7) {
			is = new ItemStack(Material.RECORD_5, MapVoting.mapVotes.get(map));
		}
		if(a % 10 == 8) {
			is = new ItemStack(Material.RECORD_4, MapVoting.mapVotes.get(map));
		}
		if(a % 10 == 9) {
			is = new ItemStack(Material.RECORD_3, MapVoting.mapVotes.get(map));
		}
		return is;
	}
	
	public static boolean compareLocationXYZ(int x, int y, int z, Location l) {
		if(l.getBlockX() == x && l.getBlockY() == y && l.getBlockZ() == z) {
			return true;
		} else {
			return false;
		}
	}
	
	public static int randomNumber(int min, int max) {
		Random rand = new Random();
		int val = rand.nextInt(max - min + 1) + min;
		return val;
	}
	
	public static int playersAlive() {
		int count = 0;
		for(Player pl : Sabotage.playersPlaying) {
			if(pl.getGameMode() == GameMode.SPECTATOR) {
				continue;
			}
			count++;
		}
		return count;
	}
	
	public static int playersDead() {
		int count = 0;
		for(Player pl : Sabotage.playersPlaying) {
			if(pl.getGameMode() == GameMode.SPECTATOR) {
				count++;
			}
		}
		return count;
	}
	
	public static void onDisable() {
		if(Sabotage.gamePhase != 0) {
			Sabotage.gamePhase = 0;
			Crate.loadCrates(Map.currentlySelectedMap, 100);
			IronBar.loadIronBars(Map.currentlySelectedMap, true);
		}
		for(Player pl : Bukkit.getOnlinePlayers()) {
			SABTools.onLeave(pl, true);
			pl.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
		}
		Bukkit.getPluginManager().getPlugin("Sabotage").getLogger().info("Till next time!");
	}
	
	public static void onEnable() {
		Bukkit.getPluginManager().getPlugin("Sabotage").getLogger().info("Starting up...");
		Bukkit.getPluginManager().getPlugin("Sabotage").getLogger().info("Loading Configuration Files...");
		for(Data d : Data.configs) {
			if(d != Data.CONFIG) {
				if(Data.CONFIG.getFileConfig().getBoolean("UseMainFolder") == true) {
					d.setFile("");
				} else {
					d.setFile(Data.CONFIG.getFileConfig().getString("UseCustomFolder"));
				}
			}
			d.createDataFile();
		}
		Sabotage.gamePhase = 0;
		Map.currentlySelectedMap = null;
		Bukkit.getPluginManager().getPlugin("Sabotage").getLogger().info("Importing Maps...");
		Map.refreshMaps(true);
		Bukkit.getPluginManager().getPlugin("Sabotage").getLogger().info("Importing Crates...");
		Crate.refreshCrates();
		Bukkit.getPluginManager().getPlugin("Sabotage").getLogger().info("Importing Bars...");
		IronBar.refreshIronBars();
		Bukkit.getPluginManager().getPlugin("Sabotage").getLogger().info("Importing Wool...");
		StatusWool.refreshSWs();
		Bukkit.getPluginManager().getPlugin("Sabotage").getLogger().info("Importing Signs...");
		TesterSign.refreshTesterSigns();
		Bukkit.getPluginManager().getPlugin("Sabotage").getLogger().info("Finishing up...");
		TesterSign.inUse = false;
		Loot.loadLoot();
		ShopItem.loadShopItems();
		
		Schedule.runChecks();
		
		if(Bukkit.getPluginManager().getPlugin("kNoCore") != null) {
			Sabotage.knc = true;
		} else {
			Sabotage.knc = false;
		}
		Bukkit.getPluginManager().getPlugin("Sabotage").getLogger().info("We good!");
	}
}
