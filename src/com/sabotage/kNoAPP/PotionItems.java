package com.sabotage.kNoAPP;

import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;

@SuppressWarnings("deprecation")
public class PotionItems {

	public static ItemStack createPotion(PotionType pt, int level) {
		Potion pot = new Potion(pt, level, false);
		pot.setSplash(false);
		return pot.toItemStack(1);
	}
}
