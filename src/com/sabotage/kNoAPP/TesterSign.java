package com.sabotage.kNoAPP;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import net.md_5.bungee.api.ChatColor;

public class TesterSign {
	public static Plugin plugin = Bukkit.getPluginManager().getPlugin("Sabotage");
	public static ArrayList<TesterSign> TSs = new ArrayList<TesterSign>();
	public static boolean inUse;
	
	private Map map;
	private int id;
	private int x;
	private int y;
	private int z;

	public TesterSign(Map map, int id, int x, int y, int z) {
		this.map = map;
		this.id = id;
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public static void createTS(Map map, int x, int y, int z) {
		FileConfiguration fc = Data.MAIN.getFileConfig();
		int i = getNextOpenID(map);
		fc.set("Maps.MapIDs." + map.getID() + ".TSs.TSIDs." + i + ".X", x);
		fc.set("Maps.MapIDs." + map.getID() + ".TSs.TSIDs." + i + ".Y", y);
		fc.set("Maps.MapIDs." + map.getID() + ".TSs.TSIDs." + i + ".Z", z);
		fc.set("Maps.MapIDs." + map.getID() + ".TSs.TSCount", fc.getInt("Maps.MapIDs." + map.getID() + ".TSs.TSCount") + 1);
		Data.MAIN.saveDataFile(fc);
		refreshTesterSigns();
	}
	
	public static void removeTS(Location l) {
		FileConfiguration fc = Data.MAIN.getFileConfig();
		for(TesterSign TS : TSs) {
			if(SABTools.compareLocationXYZ(TS.getX(), TS.getY(), TS.getZ(), l)) {
				fc.set("Maps.MapIDs." + TS.getMap().getID() + ".TSs.TSIDs." + TS.getID(), null);
				fc.set("Maps.MapIDs." + TS.getMap().getID() + ".TSs.TSCount", fc.getInt("Maps.MapIDs." + TS.getMap().getID() + ".TSs.TSCount") - 1);
			}
		}
		Data.MAIN.saveDataFile(fc);
		refreshTesterSigns();
	}
	
	public static TesterSign getTSByLocation(Location l) {
		for(TesterSign TS : TSs) {
			if(SABTools.compareLocationXYZ(TS.getX(), TS.getY(), TS.getZ(), l)) {
				return TS;
			}
		}
		return null;
	}
	
	public static boolean validTS(Location l) {
		int ax = l.getBlockX();
		int ay = l.getBlockY();
		int az = l.getBlockZ();
		for(TesterSign TS : TSs) {
			if(Data.MAIN.getFileConfig().getDouble("Maps.MapIDs." + TS.getMap().getID() + ".TSs.TSIDs." + TS.getID() + ".X") == ax && Data.MAIN.getFileConfig().getDouble("Maps.MapIDs." + TS.getMap().getID() + ".TS.TSIDs." + TS.getID() + ".Y") == ay && Data.MAIN.getFileConfig().getDouble("Maps.MapIDs." + TS.getMap().getID() + ".TS.TSIDs." + TS.getID() + ".Z") == az) {
				return true;
			}
		}
		return false;
	}
	
	public static int getNextOpenID(Map map) {
		int currentID = 1;
		while(currentID < 10000) {
			if(!Data.MAIN.getFileConfig().isSet("Maps.MapIDs." + map.getID() + ".TSs.TSIDs." + currentID)) {
				return currentID;
			}
			currentID++;
		}
		return 0;
	}
	
	public Map getMap() {
		return map;
	}
	
	public int getID() {
		return id;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public int getZ() {
		return z;
	}
	
	public static void refreshTesterSigns() {
		TSs = importTSs(Data.MAIN.getFileConfig());
	}
	
	public static ArrayList<TesterSign> importTSs(FileConfiguration fc) {
		ArrayList<TesterSign> TSs = new ArrayList<TesterSign>();
		for(Map map : Map.importMaps(fc)) {
			int tc = fc.getInt("Maps.MapIDs." + map.getID() + ".TSs.TSCount");
			for(int i=1; i<=tc; i++) {
				if(!fc.isSet("Maps.MapIDs." + map.getID() + ".TSs.TSIDs." + i)) {
					tc++;
					continue;
				}
				int x = 0;
				int y = 0;
				int z = 0;
				if(fc.isSet("Maps.MapIDs." + map.getID() + ".TSs.TSIDs." + i + ".X")) {
					x = fc.getInt("Maps.MapIDs." + map.getID() + ".TSs.TSIDs." + i + ".X");
				}
				if(fc.isSet("Maps.MapIDs." + map.getID() + ".TSs.TSIDs." + i + ".Y")) {
					y = fc.getInt("Maps.MapIDs." + map.getID() + ".TSs.TSIDs." + i + ".Y");
				}
				if(fc.isSet("Maps.MapIDs." + map.getID() + ".TSs.TSIDs." + i + ".Z")) {
					z = fc.getInt("Maps.MapIDs." + map.getID() + ".TSs.TSIDs." + i + ".Z");
				}
				
				TesterSign TS = new TesterSign(map, i, x, y, z);
				TSs.add(TS);
			}
		}
		return TSs;
	}
	
	public static void activate(Player p, int runs, long delay) {
		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
    		@SuppressWarnings("deprecation")
			public void run() {
    			if(Sabotage.gamePhase != 2) {
    				inUse = false;
    				return;
    			}
    			if(runs > 0) {
    				p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 30, 4), true);
    			}
    			if(runs == 15) {
    				inUse = true;
    				for(IronBar ib : IronBar.ironbars) {
    					if(ib.getMap() == Map.currentlySelectedMap) {
    						IronBar.loadIronBars(Map.currentlySelectedMap, true);
    					}
    				}
    			}
    			if(runs == 15 || runs == 5) {
    				SABTools.sendGameMessage(ChatColor.AQUA + p.getName() + " is being analyzed in the Saboteur Inspecter for another " + ChatColor.BLUE + runs + " seconds" + ChatColor.AQUA + ", watch for the red light.");
    			}
    			if(runs == 0) {
    				if(Sabotage.playersAvoidingDetection.contains(p) || Role.playersRole.get(p) == Role.INNOCENT) {
    					if(Sabotage.playersAvoidingDetection.contains(p)) {
    						Sabotage.playersAvoidingDetection.remove(p);
    						p.sendMessage(ChatColor.DARK_RED + "You have used your Hack Revelation!");
    					}
    					for(StatusWool sw : StatusWool.SWs) {
    						if(sw.getMap() == Map.currentlySelectedMap) {
    							p.getWorld().getBlockAt(sw.getX(), sw.getY(), sw.getZ()).setData((byte) 5);
    						}
    					}
    				} else if(Role.playersRole.get(p) == Role.DETECTIVE) {
    					for(StatusWool sw : StatusWool.SWs) {
    						if(sw.getMap() == Map.currentlySelectedMap) {
    							p.getWorld().getBlockAt(sw.getX(), sw.getY(), sw.getZ()).setData((byte) 3);
    						}
    					}
    				} else {
    					for(StatusWool sw : StatusWool.SWs) {
    						if(sw.getMap() == Map.currentlySelectedMap) {
    							p.getWorld().getBlockAt(sw.getX(), sw.getY(), sw.getZ()).setData((byte) 14);
    						}
    					}
    				}
    				for(IronBar ib : IronBar.ironbars) {
    					if(ib.getMap() == Map.currentlySelectedMap) {
    						IronBar.loadIronBars(Map.currentlySelectedMap, false);
    					}
    				}
    			}
    			
    			if(runs == -5) {
    				inUse = false;
    				for(StatusWool sw : StatusWool.SWs) {
						if(sw.getMap() == Map.currentlySelectedMap) {
							p.getWorld().getBlockAt(sw.getX(), sw.getY(), sw.getZ()).setType(Material.AIR);
							p.getWorld().getBlockAt(sw.getX(), sw.getY(), sw.getZ()).setType(Material.WOOL);
						}
					}
    				return;
    			}
    			activate(p, runs - 1, 20L);
    		}
		}, delay);
	}
}
