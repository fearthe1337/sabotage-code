package com.sabotage.kNoAPP;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class ShopItem {
	
	public static final ShopItem COMPASS = new ShopItem("Compass", "Points to the nearest player", new Role[]{Role.INNOCENT, Role.DETECTIVE, Role.SABOTEUR}, 20);
	public static final ShopItem SURPRISE = new ShopItem("Surprise", "A fake chest that explodes when opened", new Role[]{Role.SABOTEUR}, 30);
	public static final ShopItem MAP_TRACKER = new ShopItem("Map Tracker", "Gives you a map that shows all players", new Role[]{Role.INNOCENT, Role.DETECTIVE, Role.SABOTEUR}, 40);
	public static final ShopItem SPEED_II = new ShopItem("Speed II", "Gives you the speed 2 potion effect for 1 minute", new Role[]{Role.INNOCENT, Role.DETECTIVE, Role.SABOTEUR}, 40);
	public static final ShopItem SECOND_WIND = new ShopItem("Second Wind", "Gives you 4-6 hearts when you run low", new Role[]{Role.INNOCENT}, 60);
	public static final ShopItem RESPAWN = new ShopItem("Respawn", "Respawn back into the game", new Role[]{Role.INNOCENT}, 100);
	public static final ShopItem INSIGHT = new ShopItem("Insight", "The shears will show someones true role. 1 times use", new Role[]{Role.DETECTIVE}, 60);
	public static final ShopItem HACK_REVELATION = new ShopItem("Hack Revelation", "The Tester will show you as a innocent. 1 time use", new Role[]{Role.SABOTEUR}, 50);
	public static final ShopItem MARTYR = new ShopItem("Martyr", "Drop a huge bomb when you die", new Role[]{Role.SABOTEUR}, 50);
	public static final ShopItem INVISIBILITY = new ShopItem("Invisibility", "Gives you a invisiblity potion effect for 30 seconds", new Role[]{Role.SABOTEUR}, 60);

	private String itemName;
	private String desc;
	private Role[] appliesTo;
	private int cost;
	
	public static ShopItem[] rawShopItems = new ShopItem[]{COMPASS, SURPRISE, MAP_TRACKER, SPEED_II, SECOND_WIND, RESPAWN, INSIGHT, HACK_REVELATION,
			MARTYR, INVISIBILITY};
	public static ArrayList<ShopItem> shopItems = new ArrayList<ShopItem>(); 
	
	public ShopItem(String itemName, String desc, Role[] appliesTo, int cost) {
		this.itemName = itemName;
		this.desc = desc;
		this.appliesTo = appliesTo;
		this.cost = cost;
	}
	
	public String getName() {
		return itemName;
	}
	
	public String getDesc() {
		return desc;
	}
	
	public Role[] getRawApplicableRoles() {
		return appliesTo;
	}
	
	public ArrayList<Role> getApplicableRoles() {
		ArrayList<Role> roles = new ArrayList<Role>();
		for(Role r : this.getRawApplicableRoles()) {
			roles.add(r);
		}
		return roles;
	}
	
	public int getCost() {
		return cost;
	}
	
	public static ShopItem getItemByID(int reqID, Role r) {
		int id = 0;
		for(ShopItem si : shopItems) {
			if(si.getApplicableRoles().contains(r)) {
				id++;
				if(id == reqID) {
					return si;
				}
			}
		}
		return null;
	}
	
	public boolean canActivate(Player p) {
		if(Karma.getKarma(p.getName()) > this.cost) {
			return true;
		} else {
			return false;
		}
	}
	
	public void activate(Player p) {
		Karma.removeKarma(p.getName(), this.cost);
		Karma.updateKarma(p);
		if(this == ShopItem.COMPASS) {
			p.sendMessage(ChatColor.GOLD + "Right Click " + ChatColor.YELLOW + "the compass to refresh!");
			p.getInventory().addItem(ItemManagement.getCompassItem());
		}
		if(this == ShopItem.SURPRISE) {
			p.getInventory().addItem(ItemManagement.getSurpriseItem());
		}
		if(this == ShopItem.MAP_TRACKER) {
			p.getInventory().addItem(new ItemStack(Material.EMPTY_MAP, 1));
		}
		if(this == ShopItem.SPEED_II) {
			p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 1200, 1), true);
		}
		if(this == ShopItem.SECOND_WIND) {
			p.sendMessage(ChatColor.GREEN + "Second Wind is now Active!");
			Sabotage.playersWithHealthBenefits.add(p);
		}
		if(this == ShopItem.RESPAWN) {
			p.setGameMode(GameMode.SURVIVAL);
			p.teleport(Map.currentlySelectedMap.getSpawnLocation());
		}
		if(this == ShopItem.INSIGHT) {
			p.sendMessage(ChatColor.GREEN + "Insight Active and bound to your shears!");
			Sabotage.detectiveInsight.add(p);
		}
		if(this == ShopItem.HACK_REVELATION) {
			p.sendMessage(ChatColor.GREEN + "Hack Revelation Active!");
			Sabotage.playersAvoidingDetection.add(p);
		}
		if(this == ShopItem.MARTYR) {
			p.sendMessage(ChatColor.DARK_RED + "You will drop a large explosive on death!");
			Sabotage.playersWithBigBoom.add(p);
		}
		if(this == ShopItem.INVISIBILITY) {
			p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 600, 1), true);
		}
	}
	
	public static void loadShopItems() {
		ArrayList<ShopItem> shopIs = new ArrayList<ShopItem>();
		for(ShopItem si : rawShopItems) {
			shopIs.add(si);
		}
		shopItems = shopIs;
	}
}
