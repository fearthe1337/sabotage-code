package com.sabotage.kNoAPP;

import java.util.ArrayList;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;

public class Map {
	public static ArrayList<Map> maps = new ArrayList<Map>();
	public static Map currentlySelectedMap;
	
	private String name;
	private int id;
	private boolean status;
	private Location spawnLocation;
	
	public Map(String name, int id, boolean status, Location spawnLocation) {
		this.name = name;
		this.id = id;
		this.status = status;
		this.spawnLocation = spawnLocation;
	}
	
	public String getName() {
		return name;
	}
	
	public int getID() {
		return id;
	}
	
	public boolean getStatus() {
		return status;
	}
	
	public void setStatus(boolean status) {
		FileConfiguration fc = Data.MAIN.getFileConfig();
		fc.set("Maps.MapIDs." + this.getID() + ".Status", status);
		Data.MAIN.saveDataFile(fc);
		refreshMaps(false);
	}
	
	public Location getSpawnLocation() {
		return spawnLocation;
	}
	
	public void setSpawnLocation(Location spawnLocation) {
		FileConfiguration fc = Data.MAIN.getFileConfig();
		fc.set("Maps.MapIDs." + this.getID() + ".spawnLocation", spawnLocation);
		Data.MAIN.saveDataFile(fc);
		refreshMaps(false);
	}
	
	public static Map getMapByName(String name) {
		for(Map map : importMaps(Data.MAIN.getFileConfig())) {
			if(map.getName().equalsIgnoreCase(name)) {
				return map;
			}
		}
		return null;
	}
	
	public static Map getMapByID(int id) {
		for(Map map : importMaps(Data.MAIN.getFileConfig())) {
			if(map.getID() == id) {
				return map;
			}
		}
		return null;
	}
	
	public static void createMap(String map) {
		FileConfiguration fc = Data.MAIN.getFileConfig();
		int id = getNextOpenID();
		fc.set("Maps.MapIDs." + id + ".Name", map);
		fc.set("Maps.MapIDs." + id + ".Status", fc.getBoolean("General.NewMaps.EnableByDefault"));
		fc.set("Maps.MapIDs." + id + ".Crates.CrateCount", 0);
		fc.set("Maps.MapIDs." + id + ".TSs.TSCount", 0);
		fc.set("Maps.MapIDs." + id + ".IronBars.IronBarCount", 0);
		fc.set("Maps.MapIDs." + id + ".SWs.SWCount", 0);
		fc.set("Maps.MapCount", fc.getInt("Maps.MapCount") + 1);
		Data.MAIN.saveDataFile(fc);
		refreshMaps(true);
	}
	
	public static int getNextOpenID() {
		int currentID = 1;
		while(currentID < 64) {
			if(!Data.MAIN.getFileConfig().isSet("Maps.MapIDs." + currentID)) {
				return currentID;
			}
			currentID++;
		}
		return 0;
	}
	
	public static void removeMap(String map) {
		int mc = Data.MAIN.getFileConfig().getInt("Maps.MapCount");
		for(int i=1; i<=mc; i++) {
			if(!Data.MAIN.getFileConfig().isSet("Maps.MapIDs." + i)) {
				mc++;
				continue;
			}
			if(Data.MAIN.getFileConfig().getString("Maps.MapIDs." + i + ".Name").equalsIgnoreCase(map)) {
				FileConfiguration fc = Data.MAIN.getFileConfig();
				fc.set("Maps.MapIDs." + i, null);
				fc.set("Maps.MapCount", mc - 1);
				Data.MAIN.saveDataFile(fc);
			}
		}
		refreshMaps(true);
	}
	
	public static boolean validMap(String name) {
		refreshMaps(false);
		for(Map map : maps) {
			if(map.getName().equalsIgnoreCase(name)) {
				return true;
			}
		}
		return false;
	}
	
	public static void refreshMaps(boolean all) {
		maps = importMaps(Data.MAIN.getFileConfig());
		if(all) {
			MapVoting.mapVotes.clear();
			MapVoting.playersVote.clear();
		}
	}
	
	public static ArrayList<Map> importMaps(FileConfiguration fc) {
		ArrayList<Map> maps = new ArrayList<Map>();
		int mc = fc.getInt("Maps.MapCount");
		for(int i=1; i<=mc; i++) {
			if(!fc.isSet("Maps.MapIDs." + i)) {
				mc++;
				continue;
			}
			String name = fc.getString("Maps.MapIDs." + i + ".Name");
			int id = i;
			boolean status = fc.getBoolean("Maps.MapIDs." + i + ".Status");
			Location spawnLocation = null;
			
			if(fc.isSet("Maps.MapIDs." + i + ".spawnLocation")) {
				spawnLocation = (Location) fc.get("Maps.MapIDs." + i + ".spawnLocation");
			}
			
			Map map = new Map(name, id, status, spawnLocation);
			maps.add(map);
		}
		return maps;
	}
}
