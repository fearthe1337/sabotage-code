package com.sabotage.kNoAPP;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class Passes {

	@SuppressWarnings("deprecation")
	public static int getSabPasses(String name) {
		Player tON = Bukkit.getPlayerExact(name);
		if(tON == null) {
			OfflinePlayer tOFF = Bukkit.getOfflinePlayer(name);
			return Data.PLAYER.getFileConfig().getInt("Player." + tOFF.getUniqueId() + ".SaboteurPasses");
		} else {
			return Data.PLAYER.getFileConfig().getInt("Player." + tON.getUniqueId() + ".SaboteurPasses");
		}
	}
	@SuppressWarnings("deprecation")
	public static void addSabPasses(String name, int amt) {
		FileConfiguration fc = Data.PLAYER.getFileConfig();
		Player tON = Bukkit.getPlayerExact(name);
		if(tON == null) {
			OfflinePlayer tOFF = Bukkit.getOfflinePlayer(name);
			fc.set("Player." + tOFF.getUniqueId()+ ".SaboteurPasses", fc.getInt("Player." + tOFF.getUniqueId() + ".SaboteurPasses") + amt);
		} else {
			fc.set("Player." + tON.getUniqueId()+ ".SaboteurPasses", fc.getInt("Player." + tON.getUniqueId() + ".SaboteurPasses") + amt);
		}
		Data.PLAYER.saveDataFile(fc);
	}
	
	@SuppressWarnings("deprecation")
	public static void removeSabPasses(String name, int amt) {
		FileConfiguration fc = Data.PLAYER.getFileConfig();
		Player tON = Bukkit.getPlayerExact(name);
		if(tON == null) {
			OfflinePlayer tOFF = Bukkit.getOfflinePlayer(name);
			fc.set("Player." + tOFF.getUniqueId()+ ".SaboteurPasses", fc.getInt("Player." + tOFF.getUniqueId() + ".SaboteurPasses") - amt);
		} else {
			fc.set("Player." + tON.getUniqueId()+ ".SaboteurPasses", fc.getInt("Player." + tON.getUniqueId() + ".SaboteurPasses") - amt);
		}
		Data.PLAYER.saveDataFile(fc);
	}
}