package com.sabotage.kNoAPP;

import java.util.Collections;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

public class Role {
	
	public static final Role INNOCENT = new Role("Innocent", ChatColor.GREEN);
	public static final Role DETECTIVE = new Role("Detective", ChatColor.AQUA);
	public static final Role SABOTEUR = new Role("Saboteur", ChatColor.RED);
	
	public static Role[] roles = new Role[]{INNOCENT, DETECTIVE, SABOTEUR};
	
	public static HashMap<Player, Role> playersRole = new HashMap<Player, Role>();
	
	private String name;
	private ChatColor color;

	public Role(String name, ChatColor color) {
		this.name = name;
		this.color = color;
	}
	
	public String getName() {
		return name;
	}
	
	public ChatColor getColor() {
		return color;
	}
	
	public Scoreboard generateScoreboard(Player p) {
		Scoreboard sb = Bukkit.getScoreboardManager().getNewScoreboard();
		Team inno = sb.registerNewTeam(Role.INNOCENT.getName());
		Team detect = sb.registerNewTeam(Role.DETECTIVE.getName());
		Team sab = sb.registerNewTeam(Role.SABOTEUR.getName());
		Team notPlaying = sb.registerNewTeam("notPlaying");
		
		for(Player pl : Bukkit.getOnlinePlayers()) {
			if(Sabotage.playersPlaying.contains(pl)) {
				if(playersRole.get(pl) == Role.INNOCENT) {
					inno.addEntry(pl.getName());
				}
				if(playersRole.get(pl) == Role.DETECTIVE) {
					detect.addEntry(pl.getName());
				}
				if(playersRole.get(pl) == Role.SABOTEUR) {
					sab.addEntry(pl.getName());
				}
			} else {
				notPlaying.addEntry(pl.getName());
			}
		}
		
		if(this == Role.INNOCENT) {
			inno.setPrefix(ChatColor.YELLOW + "");
			inno.setDisplayName(ChatColor.YELLOW + p.getName());
			
			detect.setPrefix(Role.DETECTIVE.getColor() + "");
			detect.setDisplayName(Role.DETECTIVE.getColor() + p.getName());
			
			sab.setPrefix(ChatColor.YELLOW + "");
			sab.setDisplayName(ChatColor.YELLOW + p.getName());
			
			notPlaying.setPrefix(ChatColor.GRAY + "");
			notPlaying.setDisplayName(ChatColor.GRAY + "");
			return sb;
		}
		if(this == Role.DETECTIVE) {
			inno.setPrefix(ChatColor.YELLOW + "");
			inno.setDisplayName(ChatColor.YELLOW + p.getName());
			
			detect.setPrefix(Role.DETECTIVE.getColor() + "");
			detect.setDisplayName(Role.DETECTIVE.getColor() + p.getName());
			
			sab.setPrefix(ChatColor.YELLOW + "");
			sab.setDisplayName(ChatColor.YELLOW + p.getName());
			
			notPlaying.setPrefix(ChatColor.GRAY + "");
			notPlaying.setDisplayName(ChatColor.GRAY + "");
			return sb;
		}
		if(this == Role.SABOTEUR) {
			inno.setPrefix(Role.INNOCENT.getColor() + "");
			inno.setDisplayName(Role.INNOCENT.getColor() + p.getName());
			
			detect.setPrefix(Role.DETECTIVE.getColor() + "");
			detect.setDisplayName(Role.DETECTIVE.getColor() + p.getName());
			
			sab.setPrefix(Role.SABOTEUR.getColor() + "");
			sab.setDisplayName(Role.SABOTEUR.getColor() + p.getName());
			
			notPlaying.setPrefix(ChatColor.GRAY + "");
			notPlaying.setDisplayName(ChatColor.GRAY + "");
			return sb;
		}
		return null;
	}
	
	public static void generateRoles() {
		int detectCount = 0;
		int sabCount = 0;
		int players = Sabotage.playersPlaying.size();
		
		if(Sabotage.playersWithPasses.size() > 0) {
			for(Player pl : Sabotage.playersWithPasses) {
				if(sabCount == 0) {
					playersRole.put(pl, Role.SABOTEUR);
					sabCount++;
				} else if(players/sabCount > Data.MAIN.getFileConfig().getInt("General.GameOptions.OneSabForEveryXPlayers")) {
					playersRole.put(pl, Role.SABOTEUR);
					sabCount++;
				} else {
					Sabotage.playersWithPasses.remove(pl);
				}
			}
		}
		
		Collections.shuffle(Sabotage.playersPlaying);
		for(Player pl : Sabotage.playersPlaying) {
			if(Sabotage.playersWithPasses.contains(pl)) {
				continue;
			}
			if(detectCount == 0) {
				playersRole.put(pl, Role.DETECTIVE);
				detectCount++;
			} else if(sabCount == 0) {
				playersRole.put(pl, Role.SABOTEUR);
				sabCount++;
			} else if(players/sabCount > Data.MAIN.getFileConfig().getInt("General.GameOptions.OneSabForEveryXPlayers")) {
				playersRole.put(pl, Role.SABOTEUR);
				sabCount++;
			} else {
				playersRole.put(pl, Role.INNOCENT);
			}
		}
	}
}
