package com.sabotage.kNoAPP;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionType;

public class Loot {
	
	public static final Loot BOW = new Loot(0, 15, 1, new ItemStack(Material.BOW, 1), ChatColor.WHITE + "Bow", ChatColor.YELLOW + "Good for long range battles!");
	public static final Loot WOOD_SWORD = new Loot(16, 45, 1, new ItemStack(Material.WOOD_SWORD, 1), ChatColor.YELLOW + "Wood Sword", ChatColor.YELLOW + "Made from the finest logs!");
	public static final Loot STONE_SWORD = new Loot(46, 55, 1, new ItemStack(Material.STONE_SWORD, 1), ChatColor.GRAY + "Stone Sword", ChatColor.YELLOW + "Cut from the heart of a mountain.");
	public static final Loot LEATHER_HELMET = new Loot(56, 58, 1, new ItemStack(Material.LEATHER_HELMET, 1), ChatColor.YELLOW + "Leather Helmet", ChatColor.YELLOW + "Good on cold day.");
	public static final Loot LEATHER_CHESTPLATE = new Loot(59, 61, 1, new ItemStack(Material.LEATHER_CHESTPLATE, 1), ChatColor.YELLOW + "Leather Chestplate", ChatColor.YELLOW + "Put yo shirt on!");
	public static final Loot LEATHER_LEGGINGS = new Loot(62, 64, 1, new ItemStack(Material.LEATHER_LEGGINGS, 1), ChatColor.YELLOW + "Leather Leggings", ChatColor.YELLOW + "Who's are those..?");
	public static final Loot LEATHER_BOOTS = new Loot(65, 67, 1, new ItemStack(Material.LEATHER_BOOTS, 1), ChatColor.YELLOW + "Leather Boots", ChatColor.YELLOW + "Don't get muddy!");
	public static final Loot GOLD_HELMET = new Loot(68, 69, 1, new ItemStack(Material.GOLD_HELMET, 1), ChatColor.YELLOW + "Gold Helmet", ChatColor.YELLOW + "Oh! Shiny!");
	public static final Loot GOLD_CHESTPLATE = new Loot(70, 71, 1, new ItemStack(Material.GOLD_CHESTPLATE, 1), ChatColor.YELLOW + "Gold Chestplate", ChatColor.YELLOW + "Made with the finest gold!");
	public static final Loot GOLD_LEGGINGS = new Loot(72, 73, 1, new ItemStack(Material.GOLD_LEGGINGS, 1), ChatColor.YELLOW + "Gold Leggings", ChatColor.YELLOW + "Someone's got Disco Pants on!");
	public static final Loot GOLD_BOOTS = new Loot(74, 75, 1, new ItemStack(Material.GOLD_BOOTS, 1), ChatColor.YELLOW + "Gold Boots", ChatColor.YELLOW + "Those look uncomfortable...");
	public static final Loot FISHING_ROD = new Loot(76, 80, 1, new ItemStack(Material.FISHING_ROD, 1), ChatColor.AQUA + "Fishing Rod", ChatColor.YELLOW + "Ah! Good for catching players.");
	public static final Loot REGENERATION = new Loot(81, 85, 1, PotionItems.createPotion(PotionType.REGEN, 1), ChatColor.LIGHT_PURPLE + "Regeneration", ChatColor.YELLOW + "Drink for health.");
	public static final Loot RESISTANCE = new Loot(86, 95, 1, PotionItems.createPotion(PotionType.FIRE_RESISTANCE, 1), ChatColor.GOLD + "Fire Resistance", ChatColor.YELLOW + "Fire is scared of you, now!");
	public static final Loot SPEED = new Loot(96, 100, 1, PotionItems.createPotion(PotionType.SPEED, 1), ChatColor.AQUA + "Speed", ChatColor.YELLOW + "Not Drugs. Promise...");
	
	public static final Loot IRON_SWORD = new Loot(0, 10, 2, new ItemStack(Material.IRON_SWORD, 1), ChatColor.GRAY + "Iron Sword", ChatColor.YELLOW + "This sword glows with power!");
	public static final Loot IRON_HELMET = new Loot(11, 20, 2, new ItemStack(Material.IRON_HELMET, 1), ChatColor.GRAY + "Iron Helmet", ChatColor.YELLOW + "Glows like a crown.");
	public static final Loot IRON_CHESTPLATE = new Loot(21, 30, 2, new ItemStack(Material.IRON_CHESTPLATE, 1), ChatColor.GRAY + "Iron Chestplate", ChatColor.YELLOW + "The strongest armour around!");
	public static final Loot IRON_LEGGINGS = new Loot(31, 40, 2, new ItemStack(Material.IRON_LEGGINGS, 1), ChatColor.GRAY + "Iron Leggings", ChatColor.YELLOW + "Someone's in the kitchen..!");
	public static final Loot IRON_BOOTS = new Loot(41, 50, 2, new ItemStack(Material.IRON_BOOTS, 1), ChatColor.GRAY + "Iron Boots", ChatColor.YELLOW + "*click* *clank*");
	public static final Loot REGENERATION2 = new Loot(51, 60, 2, PotionItems.createPotion(PotionType.REGEN, 2), ChatColor.LIGHT_PURPLE + "Regeneration", ChatColor.YELLOW + "Powerful healing!");
	public static final Loot RESISTANCE2 = new Loot(61, 70, 2, PotionItems.createPotion(PotionType.FIRE_RESISTANCE, 2), ChatColor.GOLD + "Fire Resistance", ChatColor.YELLOW + "Never catch on fire again!");
	public static final Loot SPEED2 = new Loot(71, 80, 2, PotionItems.createPotion(PotionType.SPEED, 2), ChatColor.AQUA + "Speed", ChatColor.YELLOW + "Run like Bolt!");
	public static final Loot FISHING_ROD2 = new Loot(81, 85, 2, new ItemStack(Material.FISHING_ROD, 1), ChatColor.AQUA + "Fishing Rod", ChatColor.YELLOW + "Ah! Good for catching players.");
	public static final Loot BOW2 = new Loot(86, 90, 2, new ItemStack(Material.BOW, 1), ChatColor.WHITE + "Bow", ChatColor.YELLOW + "Good for long range battles!");
	public static final Loot TNT = new Loot(91, 100, 2, new ItemStack(Material.TNT, 1), ChatColor.RED + "TNT", ChatColor.YELLOW + "Light and run!");
	
	public static Loot[] rawLootItems = new Loot[]{BOW, WOOD_SWORD, STONE_SWORD, LEATHER_HELMET, LEATHER_CHESTPLATE, LEATHER_LEGGINGS, LEATHER_BOOTS, 
			GOLD_HELMET, GOLD_CHESTPLATE, GOLD_LEGGINGS, GOLD_BOOTS, FISHING_ROD, REGENERATION, RESISTANCE, SPEED, IRON_SWORD, IRON_SWORD,
			IRON_HELMET, IRON_CHESTPLATE, IRON_LEGGINGS, IRON_BOOTS, REGENERATION2, RESISTANCE2, SPEED2, FISHING_ROD2, BOW2, TNT};
	
	private int min;
	private int max;
	private int level;
	private ItemStack item;
	private String name;
	private String lore;

	public static ArrayList<Loot> lootItems = new ArrayList<Loot>();
	
	public Loot(int min, int max, int level, ItemStack item, String name, String lore) {
		this.min = min;
		this.max = max;
		this.level = level;
		this.item = item;
		this.name = name;
		this.lore = lore;
	}
	
	public int getMin() {
		return min;
	}
	
	public int getMax() {
		return max;
	}
	
	public int getLevel() {
		return level;
	}
	
	public ItemStack getItem() {
		return item;
	}
	
	public String getName() {
		return name;
	}
	
	public String getLore() {
		return lore;
	}
	
	public static void loadLoot() {
		ArrayList<Loot> loots = new ArrayList<Loot>();
		for(Loot l : rawLootItems) {
			loots.add(l);
		}
		lootItems = loots;
	}
	
	public static Loot selectLoot(int level) {
		int random = SABTools.randomNumber(0, 100);
		for(Loot l : lootItems) {
			if(l.getMin() <= random && random <= l.getMax() && l.getLevel() == level) {
				return l;
			}
		}
		return null;
	}
	
	public ItemStack generateItem() {
		ItemStack is = this.item;
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(this.getName());
		ArrayList<String> lores = new ArrayList<String>();
		lores.add(this.lore);
		lores.add(ChatColor.DARK_GRAY + "ID: " + this.getMin() + "-" + this.getMax() + " " + ChatColor.GRAY + "||" + ChatColor.DARK_GRAY + " Level: " + this.getLevel());
		im.setLore(lores);
		is.setItemMeta(im);
		return is;
	}
}
