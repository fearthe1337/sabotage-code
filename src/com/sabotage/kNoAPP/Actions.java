package com.sabotage.kNoAPP;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

public class Actions implements Listener {

	public static ArrayList<Block> trapChest = new ArrayList<Block>();
	
	@EventHandler
	public void onPlayerClick(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if(Sabotage.playersPlaying.contains(p) && Sabotage.gamePhase != 0 && Sabotage.gamePhase != 3 && p.getGameMode() != GameMode.SPECTATOR) {
			if(e.getHand() == EquipmentSlot.HAND) {
				if(e.getAction() == Action.RIGHT_CLICK_BLOCK) {
					Block b = e.getClickedBlock();
					if(trapChest.contains(b)) {
						p.getWorld().getBlockAt(b.getLocation()).setType(Material.AIR);
						TNTPrimed tnt = p.getWorld().spawn(b.getLocation(), TNTPrimed.class);
						tnt.setYield(3F);
						tnt.setFuseTicks(20);
						tnt.setIsIncendiary(true);
						trapChest.remove(b);
					}
				}
				if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
					if(p.getInventory().getItemInMainHand() != null) {
						ItemStack is = p.getInventory().getItemInMainHand();
						if(is.isSimilar(ItemManagement.getCompassItem())) {
							double nearest = 10000;
							Player t = null;
							for(Player pl : Sabotage.playersPlaying) {
								if(pl.getGameMode() != GameMode.SPECTATOR) {
									if(p.getLocation().distance(pl.getLocation()) < nearest && p != pl) {
										nearest = p.getLocation().distance(pl.getLocation());
										t = pl;
									}
								}
							}
							try {
								p.setCompassTarget(t.getLocation());
							} catch(NullPointerException ex) {
								p.sendMessage(ChatColor.RED + "No targets!");
								return;
							}
							p.sendMessage(ChatColor.YELLOW + "Tracking Player " + ChatColor.GOLD + t.getName() + ChatColor.YELLOW + "...");
							return;
						}
					}
				}
			}
		}
	}
	
	@EventHandler
	public void onPlayerPlaceBlock(BlockPlaceEvent e) {
		Player p = e.getPlayer();
		if(Sabotage.playersPlaying.contains(p) && Sabotage.gamePhase != 0 && Sabotage.gamePhase != 3 && p.getGameMode() != GameMode.SPECTATOR) {
			if(p.getInventory().getItemInMainHand() != null) {
				ItemStack is = p.getInventory().getItemInMainHand();
				if(is.isSimilar(ItemManagement.getCompassItem())) {
					Block b = e.getBlockPlaced();
					for(Crate c : Crate.crates) {
						if(c.getMap().getName() == Map.currentlySelectedMap.getName()) {
							if(SABTools.compareLocationXYZ(c.getX(), c.getY(), c.getZ(), b.getLocation()) == true) {
								p.sendMessage(ChatColor.RED + "You cannot place a Surprise here!");
								e.setCancelled(true);
								return;
							}
						}
					}
					for(IronBar ib : IronBar.ironbars) {
						if(ib.getMap().getName() == Map.currentlySelectedMap.getName()) {
							if(SABTools.compareLocationXYZ(ib.getX(), ib.getY(), ib.getZ(), b.getLocation()) == true) {
								p.sendMessage(ChatColor.RED + "You cannot place a Surprise here!");
								e.setCancelled(true);
								return;
							}
						}
					}
					e.setCancelled(false);
					p.sendMessage(ChatColor.DARK_RED + "Armed!");
					trapChest.add(b);
				}
			}
		}
	}
}
